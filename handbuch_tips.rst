Tips für Handbücher
===================

.. admonition:: Merke 

   Jeder kann schreiben, aber nicht jeder kann so schreiben, dass ihn jeder versteht.
   
**Merkmale eines benutzerfreundlichen Stils**

Benutzerfreundlich heißt

- korrekt und unzweifelhaft
- in einer Weise geschrieben, die es einfach macht

  - die Information geistig zu verarbeiten
  - nach den gegebenen Anweisungen zu handeln
  - die gegebenen Informationen zu behalten
  

Vergleichen Sie unter diesem Aspekt die folgenden Benutzerhandbücher

- :download:`DVD Player <figure/handbuch_beispiele/handbuch_dvd_player.pdf>`.
- :download:`Word 2010 Product Guide <figure/handbuch_beispiele/Microsoft_Word_2010_Product_Guide.pdf>`.
- :download:`Motorola Moto G <figure/handbuch_beispiele/handbuch_motorola.pdf>`.
- :download:`PPT Schnellstarthandbuch <figure/handbuch_beispiele/PowerPoint2013_Schnellstarthandbuch.PDF>`.
- :download:`Minihandbuch Word 2010 <figure/handbuch_beispiele/h43-minihandbuch-word-2010.pdf>`.
- :download:`Word 2013 Schnellstart <figure/handbuch_beispiele/Word2013_Schnellstarthandbuch.PDF>`.




  	
.. only:: latex

    .. raw:: latex

       \newpage	

  
Allgemeine Prinzipien
---------------------


Einfach und Dumm
################

.. topic:: Merke

	Forget what you learned at school.
	You are NOT writing an essay. You DON'T have to impress your teacher.
	Provide information that EVERYBODY can understand -- even readers who:
	
	- don't speak the document's language as their first language
	- aren't sitting in a silent office but who, for example, are standing in a noisy production hall
	- don't have much time
	- are frustrated because they didn't succeed without reading the manual

	
	So **keep it simple and stupid** (KISS principle)
		
	- Write short sentences
	- Use simple grammar
	- Use simple words
		
	Plain language is NO evidence of poor education. Plain language is the foundation of clear user assistence.
	

.. admonition:: NEIN
		
	If you want to exert influence of the contents of  document, access the submenu item **Edit** in the **File** menu after having opened the document file sucessfully
		
.. admonition:: JA
	
	.. ifconfig:: Antwort == True

		To edit a document:
		
		#. Open the document file
		#. Choose :kbd:`File > Edit`.
	
	.. ifconfig:: Antwort == False

		sondern ..
 	
.. admonition:: NEIN
		
	Congratulations for buying this sophisticated, highly effective phone, which has been designed with your vital communication needs in mind.
		
.. admonition:: JA
	
	.. ifconfig:: Antwort == True

		You can use this phone to make phone calls
		
	
	.. ifconfig:: Antwort == False

		sondern ..
	
	
	
.. admonition:: TOP
	
	Leave out this sentence completely because it doesn't provide any useful information



.. only:: latex

    .. raw:: latex

       \newpage

       
Spreche zu dem Leser
#####################

- Rede den Leser direkt an. "Sie können ... ". Dies erhöht den Aufmerksamkeitsgrad.
- Verwenden Sie im Deutschen die direkte Anrede mit "Sie"
- Gebrauche kein Passiv 
- Sprich nicht über den Benutzer ("Benutzer können ... ")
- Benutze kein "Man"

- Habe keine Angst, einen Befehlston zu benutzen.  Der Benutzer erwartet klare Anweisungen


.. admonition:: NEIN
		
	- Der Knopf muss gedrückt werden
	- Der Knopf muss vom Benutzer gedrückt werden
	- Benutzer müssen den Knopf drücken
	- Man muss den Knopf drücken
		
.. admonition:: JA
	
	.. ifconfig:: Antwort == True

		Drücke den Knopf
	
	.. ifconfig:: Antwort == False

		sondern ..

.. admonition:: NEIN
		
	- Der *Druck*-Dialog gibt die Möglichkeit die Druckereinstellungen zu ändern
	- Es wird empfohlen ein abgeschirmtes Kabel zu verwenden.
	- Man kann das Passwort auch ändern.
	- Es ist auch möglich, das Passwort zu ändern.
	- Das Passwort kann auch geändert werden.
	- Das Passwort lässt sich auch ändern.
	
	- Den Bildschirm reinigt man am besten mit klarem Wasser.
	- Der Bildschirm wird am besten mit klarem Wasser gereinigt.
	
		
.. admonition:: JA
	
	.. ifconfig:: Antwort == True

		- Im Druck-Dialog können Sie die Drucker-Einstellungen ändern.
		- Wir empfehlen ein abgeschirmetes Kabel.
		- Sie können Ihr Passwort auch ändern.
		
		- Den Bildschirm reinigen Sie am besten mit klarem Wasser.
		- Reinigen Sie den Bildschirm am besten mit klarem Wasser.
		
		
	.. ifconfig:: Antwort == False

		sondern ..


	
.. only:: latex

    .. raw:: latex

       \newpage	
       
       
	



Sei genau
#########

Benutzers eines Handbuches erwarten klare Angaben. Der Handbuchschreiber weiß was er meint, damit ist aber nicht klar, dass auch die Leserschaft das Gleiche darunter versteht. Bedenken Sie auch, dass viele Leser ein Handbuch nicht komplett durcharbeiten und sie somit nur einen Teil des Ganzen sehen.

Ungenaue Aussagen führen

- zu Unsicherheit und wenig Vertrauen seitens der Leserschaft
- zu Missverständnissen und Fehlern
- zu falchen Informationen


.. admonition:: NEIN

	#. Wenn Sie alle Felder korrekt ausgefüllt haben, sollte das Ergebnisfenster erscheinen
	#. Die Durchführung der Aktion  sollte schnell erfolgen 
	#. Das Programm kann auch WORD-Dateien, etc. importieren
	#. Falls notwendig, schalten Sie das Licht an
	#. zwischen 7 und 11
	#. Wir werden ihre Waschmaschine erfolgreich installieren
	#. Drücken Sie eine beliebige Taste
	
	
.. admonition:: JA

	.. ifconfig:: Antwort == True
	
		#. Wenn Sie alle Felder korrekt ausgefüllt haben, erscheint das Ergebnisfenster.	
		#. Die Aktion muss in einer Minute durchgeführt sein.
		#. Das Programm kann WORD-, PDF-, XML-Dateien importieren
		#. Wenn es dunkel ist und die Lichter ausgeschaltet sind, schalten Sie das Licht an.
		#. Unklar: Von 7 bis 11
		#. Wir werden ihre Wachmaschine auspacken, konfigurieren, anschließen
		#. Drücke :kbd:`RETURN`

	.. ifconfig:: Antwort == False
	
		sondern ..
		

		
		
.. warning:: Problemwort "beziehungsweise"

	Eine häufige Quelle für schwammige  Sätze ist das Wort *beziehungsweise*. Oft ist es nur schwer nachvollziehbar, worin die Beziehung besteht.
	
	#. Entweder meinen Sie *und*. **Dann sagen Sie das auch so**. 
	#. Oder Sie meinen *oder*.  **Dann sagen Sie das auch so**.
	#. Oder Sie meinen *genauer gesagt*. **Dann sagen Sie es genauer**.
	#. Oder Sie meinen eigentlich gar nichts und *beziehungsweise* ist nur ein Füllwort. **Dann lassen Sie es weg**.  

	
	.. admonition:: NEIN

		- Füllen Sie benzin und Wasser in den Tank bzw. in den Vorratsbehälter der Wischanlage
	
	.. admonition:: JA
	
		.. ifconfig:: Antwort == True
		
			- Füllen Sie Benzin in den Tank, und füllen Sie Wasser in den Vorratsbehälter der Wischanlage.
			
		.. ifconfig:: Antwort == False
		
			sondern ..
			
	.. admonition:: Besser
	
		.. ifconfig:: Antwort == True
		
			#. Füllen Sie Benzin in den Tank
			#. Füllen Sie Wasser in den Vorratsbehälter der Wischanlage
			
	**Typische Beispiele für vage Ausdrücke im Deutschen**
	
	- bzw.
	- einige
	- entspechende 
	- es 
	- etc.
	- eventuell
	- gegebenenfalls
	- können
	- man
	- Objekt
	- sollen
	- und/oder
	- usw. 
	- ziemlich
	
	
	.. admonition:: NEIN

		Sichern Sie Ihre Daten täglich. Am Wochenende können Sie gegebenenfalls darauf verzichten
	
	.. admonition:: JA
	
		.. ifconfig:: Antwort == True

			**Sichern Sie Ihe Daten nach jedem Arbeitstag**
			
		.. ifconfig:: Antwort == False
		
			sondern ..
		


	
	
	
.. only:: latex

    .. raw:: latex

       \newpage	
       
	
Sei kurzgefasst (2.1.5)
########################


.. warning::

	Jedes gesparte Wort und Zeichen ist ein Schritt hin zu mehr Klarheit. Die einzige Ausnahme zu dieser Regel ist: Sei nicht kurzgefasst auf Kosten der Klarheit
	
	

.. admonition:: NEIN

	#. Im Rahmen der technischen Weiterentwicklung konnte die Motorleistung um 5 kW gesteigert werden.
	#. Wählen Sie einen Kalendermonat. 
	#. Stellen Sie den Sitz in eine senkrechte Position.
	
	
.. admonition:: JA

	.. ifconfig:: Antwort == True
	
		#. Die Motorlesitung wurde um 5 kW gesteigert oder Der Motor leistet jetzt 55 kW statt zuvor 50 kW
		#. Wählen Sie den Monat.
		#. Stellen Si eIhren Sitz senkrecht

	.. ifconfig:: Antwort == False
	
		sondern ..
		
	
	

.. only:: latex

    .. raw:: latex

       \newpage	


Sei einheitlich
##################

.. warning::

	Ein einheitliches Dokument ist ein leicht zu lesendes Dokument. Der Leser kann sich auf den Inhalt konzentrieren. Die EInheitlichkeit wird erreicht durch:
	
	- einheitliches Lyout und Formatierung
	- gleichbleibende Strukturierung und Ausdrücke
	
	Einheitlichkeit sollte auch zwischen Dokumenten erreicht werden

	
.. admonition:: NEIN

	Dokuementation für Produkt A
	
	- Konfiguration des Produktes
	- Erste Schritte
	- Benutzung des Produktes
	- Technische Referenz
	
	
	Dokuementation für Produkt B
	
	- Installation des Produktes
	- Einstieg in die Benutzung
	- Benutzerhandbuch
	- Entwicklerhandbuch
	
	
.. admonition:: JA

	.. ifconfig:: Antwort == True
	
		Dokumentation für Produkt A
	
		- Installation
		- Einstieg
		- Benutzerhandbuch
		- Entwicklerhandbuch
	
	
	Dokumentation für Produkt B
	
		- Installation
		- Einstieg
		- Benutzerhandbuch
		- Entwicklerhandbuch
		
		
	.. ifconfig:: Antwort == False
	
		sondern ..
		
		
	

.. only:: latex

    .. raw:: latex

       \newpage	


Sei gleichbleibend (2.1.7)
###########################

.. only:: latex

    .. raw:: latex

       \newpage	


Sei positiv
#############


.. only:: latex

    .. raw:: latex

       \newpage	


Nimm die Gegenwartsfom
########################

.. only:: latex

    .. raw:: latex

       \newpage	


Aktiv statt passiv
####################




.. only:: latex

    .. raw:: latex

       \newpage	


Vermeide Bewertungen
#####################



.. only:: latex

    .. raw:: latex

       \newpage	


Sage kein "Bitte"
##################


.. only:: latex

    .. raw:: latex

       \newpage	

Writing topics
----------------------



.. only:: latex

    .. raw:: latex

       \newpage	


wWriting "concept" topics
########################



.. only:: latex

    .. raw:: latex

       \newpage	

       
writing "task" topics
#####################


.. only:: latex

    .. raw:: latex

       \newpage	


writing reference topics
##########################


.. only:: latex

    .. raw:: latex

       \newpage	
       

Abschnitte schreiben
----------------------



Vermische keine Themen (2.3.1
###############################


.. only:: latex

    .. raw:: latex

       \newpage	

Add Labels (Subheadings)
#########################


.. only:: latex

    .. raw:: latex

       \newpage	


Abläufe beschreiben (2.3.3)
#############################


.. only:: latex

    .. raw:: latex

       \newpage	

Listen 
########

.. only:: latex

    .. raw:: latex

       \newpage	



Tabellen
#########


.. only:: latex

    .. raw:: latex

       \newpage	


Warnungen
##########


.. only:: latex

    .. raw:: latex

       \newpage	


Bemerkungen
############


.. only:: latex

    .. raw:: latex

       \newpage	


Tips
#######


.. only:: latex

    .. raw:: latex

       \newpage	

       
       
Beispiele
############


.. only:: latex

    .. raw:: latex

       \newpage	

       
       
Referenzen und Links
####################


.. only:: latex

    .. raw:: latex

       \newpage	

Sätze schreiben (2.4)
----------------------


Kurze Sätze
#############



.. only:: latex

    .. raw:: latex

       \newpage	

Hauptaussage in den HAUPTSATZ
#################################

.. only:: latex

    .. raw:: latex

       \newpage	



Vermeide Nebensätze und Verschachtelungen
##########################################



.. only:: latex

    .. raw:: latex

       \newpage	



Reiehnfolge der Wörter (2.4.6)
################################






