Handbuch und Dokumentation
==========================

Applikationen
-------------

Schreiben Sie ein Handbuch für unbedarfte Erstbenutzer zu einem der folgenden Programme. Schaffen Sie darin für den Leser einen Überblick über die wesentlichen Grundfunktionen.

- Irfan View
- CamStudio
- ToDoList
- TaskCoach
- Calibre
- MobaXterm (Personal)
- FreeMind
- OwnCloud-Client
- DropBox
- JAlbum
- Reveal.js 
- Impress.js


Erstellen Sie die Dokumente für die Medien

  - Print (pdf, A4)
  - Web (html/xhml)
  - Mobil (epub/mobi)

  
.. only:: latex

    .. raw:: latex

       \newpage

Markup Sprachen
----------------

Schreiben sie eine Dokumentation zu einer der folgenden Textauszeichnungssprachen. 

- Markdown  
- Textile
- Rest / Sphinx  
- Asciidoc
- LaTeX
- (Doconce)
- WIKI-Markup

  - mediawiki    
  - dokuwiki

Befolgen Sie bei Ihrer Arbeit folgende Regeln
  
- Verschaffen Sie sich einen Überblick über die jeweilige Alternative zu herkömmlichen Textverarbeitungssystemen; installieren Sie das System auf Ihrem eigenen Rechner.
- Vergleichen Sie die Formatierungs- und Strukturierungsmöglichkeiten (z.B. fett/kursiv, Überschriften verschiedener Ebenen, Aufzählungen, Listen, Tabellen, Bilder ...) ihrer Auszeichnungssprache mit den Fähigkeiten der gängigen Standard-Textverarbeitungen. 
- Welche Hard- und Softwarevoraussetzungen sind für die Benutzung gegeben?
- In welchem typischen Arbeitsumfeld würde man die Sprache benutzen?
- Welche "Ökosysteme" haben sich um die jeweilige Sprache gebildet?
- Für welche Zwecke ist die Sprache nicht geeignet?


- Benutzen Sie zum Schreiben der Doku die jeweilige Auszeichnungssprache.
- Versuchen Sie, ihre Aussagen (Content) in einer ansprechenden Form in folgenden Ausgabemedien zu erzeugen:
    
  - print (pdf)
  - web (html/xhml)
  - mobil (epub/mobi)

