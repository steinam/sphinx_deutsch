Aufgaben
=========

Projektstart
-------------

#. Erläutern Sie die Gründe, warum in den letzten Jahrzehnten Projektmanagement an Bedeutung gewonnen hat
#. Erläutern Sie 4 charakteristische Eigenschaften von Projekten
#. Was ist mit dem magischen Dreieck gemeint?
#. Klare und einheitlich verstandene Projektziele sind wichtig für den Projekterfolg. Bitte stellen   
   Sie dar, in welcher Weise und mit welchen Methoden Sie als Projektleiter dafür sorgen, dass die Projektziele eine gute Grundlage für die weitere Planung und Durchführung bilden. 
#. In Projekten werden unterschiedliche Phasenkonzepte eingesetzt. Erläutern Sie ein allgemeines 
   Phasenkonzept. 
#. Erläutern Sie anhand von zwei Merkmalen, worin die Besonderheiten der Projektorganisation 
   gegenüber der „regulären“ Organisation zu sehen sind.  Welche drei Grundformen der Projektorganisation lassen sich unterscheiden? 
#. Bitte benennen Sie vier Arten von Projektbeteiligten und skizzieren Sie ihre jeweiligen Aufgaben. 
#. Welche Aufgaben hat im Allgemeinen der Projektleiter? Nennen und beschreiben Sie mindestens fünf. 


.. raw:: latex

	\newpage

Übungsfirma Teufel GmbH
-----------------------

Die Firma Teufel GmbH ist ein mittelständischer Anbieter von Maschinen und Geräten für die 
Industrie. Die Firma beschäftigt rund 200 Mitarbeiter. Jakob Teufel hat die Firma vor 30 Jahren 
gegründet. Letztes Jahr hat sein Sohn Johann Teufel die Geschäftsführung übernommen. Dieser will 
nach der Übernahme nun einiges im Betrieb anders machen. Er hat beobachtet, dass die 
Auftragsabwicklung ziemlich handgestrickt gemacht wird, wodurch in Folge viele der Aufträge zu spät 
fertig, die geplanten Kosten überschritten und viele Kunden verärgert werden. 
In einer Besprechung mit den Abteilungsleitern kündigt er die Entwicklung eines einheitlichen 
Projektmanagement-Konzepts für alle Auftragsprojekte an. Er begründet diesen Schritt mit dem 
starken Konkurrenzdruck und der Notwendigkeit, schneller und mit noch besseren Produkten als 
bisher auf dem Markt zu sein. Außerdem weist er darauf hin, dass alte Produkte sich immer 
schlechter verkaufen und ein großer Teil des Umsatzes mit Produkten gemacht wird, die jünger als 
sechs Jahre sind. 

Um Umsatz und Gewinn des Unternehmens zu steigern, die Existenz des Unternehmens und damit 
die Arbeitsplätze zu sichern, will er ein systematisches Projektmanagement einführen. Dadurch 
sollen die Produktentwicklungszeiten verkürzt, mehr Innovationskraft und Produktqualität sowie 
höhere Deckungsbeiträge der einzelnen Produkte erreicht werden. Als Ergebnis soll auch die 
Kundenzufriedenheit erhöht werden. 
Konkret stellt er sich vor, dass ein Projektmanagement-Handbuch für das Unternehmen erarbeitet 
wird, an dem sich zukünftig die Projektabwicklung orientieren soll; er möchte eine 
Projektmanagement-Software einführen und er will eine ausreichende Zahl an Mitarbeitern in 
Projektmanagement schulen lassen.  
Die Projektvorbereitung hat er an seinen Assistenten Joachim Jochen übertragen. Dieser ruft für 
diese Aufgabe ein kleines Team zusammen. Stellen Sie sich vor, Sie seien dieses Team. 

a) Projektziele bestimmen 
   Formulieren Sie bitte grob die Projektziele hinsichtlich Leistung, Kosten und Terminen. Das Leistungsziel formulieren Sie genauer als oben von Herrn Engels genannt. Die Projektdauer und die Projektkosten müssen Sie grob schätzen. Denken Sie auch an die die Kosten des Einsatzes von eigenen Mitarbeitern. 

b) Erfolgskriterien festlegen 
   Nach 3 Jahren will Herr Engels den Erfolg des Projektes evaluieren. Welche Größen kann er dafür heranziehen? Formulieren Sie solche Messkriterien, mit der der Erfolg des Projektes bewertet werden kann. 
   
   
c) Erstellen Sie einen Projektstrukturplan für das Projekt. Formulieren Sie einen alternativ 
   möglichen PSP und wägen Sie deren jeweiligen Vor- und Nachteile gegeneinander ab.    
   
   
   
d) Sehen Sie sich folgende Liste mit Aufgaben innerhalb eines Projektes an. Überlegen Sie bitte, 
   wer für die einzelnen Arbeiten die Verantwortung übernehmen sollte:  

   A: Auftraggeber;  
   L: Lenkungsausschuss;  
   P: Projektleiter;  
   G: Projektgruppe;  
   F: herangezogene interne oder externe Fachleute. 
   
   
   
   ::
   	   
   	   
   	      =================================================================
   	     |    Aufgaben                             |  Verantwortlich       |
   	      =================================================================
   	     |  Projektauftrag formulieren             |                       |
   	     |_________________________________________________________________|
   	     |  Projektleiter ernennen                 |                       |
   	     |_________________________________________________________________|
   	     |  Projektmitglieder festlegen            |                       |
   	     |_________________________________________________________________|
   	     |  Projektorganisation (reine PO, Matrix, |                       |
   	     |   etc. ..) festlegen                    |                       |
   	     |_________________________________________________________________|
   	     |  Problem und Rahmenbedingung definieren |                       |
   	     |_________________________________________________________________|
   	     |  Ziele des Projektes festlegen          |                       |
   	     |_________________________________________________________________|
   	     |  Projektbeschreibung erstellen          |                       |
   	     |_________________________________________________________________|
   	     |  Ressourcen (Finanzmittel, Personal)    |                       | 
   	     |  zur Verfügung stellen                  |                       |
   	     |_________________________________________________________________|
   	     |  Projekt strukturieren                  |                       |
   	     |_________________________________________________________________|
   	     |  Tätigkeitsliste aufstellen             |                       |
   	     |_______________________________________ _________________________|
   	     |  Kostenplan aufstellen                  |                       |
   	     |_________________________________________________________________|
   	     |  Information u. Doku organisieren       |                       |
   	     |_________________________________________________________________|
   	     |  Wirtschaftlichk.  beurteilen           |                       |
   	     |_________________________________________________________________|
   	     |  Entscheidungen Projektdurchführung     |                       |
   	     |  fällen                                 |                       |
   	     |_________________________________________________________________|
   	     |  Kosten, Termine, Schritte des Projektes|                       | 
   	     |  überwachen                             |                       |
   	     |_________________________________________________________________|
   	     |  Projekt abschließen, Ergebnis          |                       | 
   	     |  dokumentieren                          |                       |
   	     |_________________________________________________________________|
   
   
 
   	     
.. raw:: latex

	\newpage   	     
   	     
Projektstrukturplan
-------------------

Die Firma Meditech GmbH plant ihre betriebliche Informationsverarbeitung auszulagern. Als 
Projektleiter für diese Maßnahmen ist es Ihre Aufgabe, die einzelnen Schritte bis zur Vorbereitung 
der Vertragsverhandlung zu planen

- Das Projekt startet mit einer Kickoff-Meeting aller Beteiligten (Dauer: 1 
  Tag), in der die Ziele und Vorgehensweise im Projekt vereinbart werden. Neben Ihnen nehmen noch 5 weitere Personen aus Ihrem Unternehmen teil. 
- Im Anschluss daran realisieren zwei Ihrer Mitarbeiter die Aufnahme der 
  Ist-Situation. Dabei erfolgt zuerst eine Inventarisierung der Hardware (Aufwand: 14 Mitarbeitertage (MT)), dann die Katalogisierung der installierten Software-Systeme (Aufwand: 8 MT) und dann die Dokumentation des Netzwerkes mit allen Verbindungen zu den Unternehmensstandorten. Die zwei Mitarbeiter benötigen für letztere Aufgabe 3 Kalendertage. 
- Die detaillierte Beschreibung der Anforderungen an die Betriebsfunktionen
  erstellen Sie parallel zur Aufnahme der Ist-Situation (Dauer: 12 Tage). 
- Der nächste Schritt ist die Ausschreibung. Dabei bilden die Ist-Aufnahme und 
  die Beschreibung der 
  Anforderungen die Voraussetzungen. Die Ausschreibungsunterlagen erarbeiten Sie als Projektleiter an 4 Tagen gemeinsam mit einem Mitarbeiter der Einkaufsabteilung. Der anschließende Versand der Ausschreibung an die potenziellen Anbieter wird durch die Projektassistentin vorgenommen (Aufwand: 1 MT). 
- Die Frist für die Angebotsabgabe beträgt 10 Tage. Nach Ablauf dieser Frist 
  erfolgt die Auswertung der eingegangenen Angebote. 
- Bei der Angebotsvorauswahl untersuchen Sie mit 2 Mitarbeitern die Angebote und 
  Outsourcing-Konzeptionen hinsichtlich Ihrer fachlichen und kommerziellen Ziele (Aufwand: 30 MT). 
- Das Ergebnis ist eine Liste der Anbieter, die zur Angebotspräsentation 
  eingeladen werden.  Die Präsentationen von insgesamt 5 Anbietern dauern jeweils einen Tag und finden an  aufeinanderfolgenden Tagen statt. Teilnehmer an diesen Präsentationen sind der Projektleiter, der Leiter der Einkaufsabteilung sowie die zwei Geschäftsführer und die Projektassistentin (Aufwand: 25 MT). 
- Die Angebotsauswahl erfolgt im Anschluss an die Präsentationen. Hier werden 
  zwei Unternehmen aufgefordert ein konkretes Angebot abzugeben. Die Kommunikation mit den jeweiligen Unternehmen übernehmen Sie zusammen mit Ihrer Projektassistentin (Aufwand: 8 MT). 
- Nach Angebotspräzisierung durch die Anbieter prüfen Sie zusammen mit dem 
  Leiter der Einkaufsabteilung die Angebote und entscheiden nach mehreren Gesprächen über eine zukünftige Outsourcing-Partnerschaft (Dauer: 7 Tage, Aufwand für beide: 2 MT). Die anschließenden Vertragsverhandlungen sind nicht mehr Bestandteil Ihrer Projektplanung. 
  
  a) Erstellen Sie  einen Projektstrukturplan und eine Vorgangsliste

  b) Erstellen Sie die Projektterminplanung als Netzplan.

  c) Ermitteln Sie den kritischen Weg und nennen oder markieren Sie die   
     Vorgänge, die auf dem kritischen Weg liegen.  
  d) Nach wie vielen Arbeitstagen kann das Projekt abgeschlossen werden? 
  e) Welche Kosten werden für dieses Ausschreibungsverfahren laut Planung 
     anfallen, wenn für jeden Mitarbeiter ein Tagessatz von durchschnittlich 500 € angesetzt wird? 
   
  
   
**Lösung**

.. image:: figure/projekt/psp_meditech_it.jpg

.. image:: figure/projekt/vl_meditech_it.jpg

   
   

.. raw:: latex

	\newpage   
   
	
	
	
Netzplan Meditech
--------------------------
   
.. sidebar:: Vorlage

	
	:download:`Vorlage Balkendiagramm <figure/projekt/balkendiagramm_vorlage.jpg>`
	


Im Rahmen der Einführung des Projektmanagement-Handbuchs bei der Meditech GmbH soll eine 
abteilungsübergreifende Schulung durchgeführt werden. Herr Schulz, Leiter der Fortbildung, erhält 
von der Geschäftsführung den Auftrag, diese betriebsinterne Maßnahme vorzubereiten und 
durchzuführen. Dazu hat er nachfolgende Liste der notwendigen Arbeitspakete erstellt. 


::
	
	Vorgangsliste 
  
	
	Nr  Vorgang                         Vorgänger     Nachfolger  Dauer in Tagen 

	1   Schulungskonzept erstellen          -            2/10       5 
	2   Seminarplan erstellen               1            3/5        2 
	3   Seminarraum buchen                  2            4          1 
	4   Seminarraum herrichten              3            8          1 
	5   Referenten auswählen                2            6          3 
	6   Schulungsunterlagen erstellen       5            7          7 
	7   Schulungsunterlagen drucken         6            8          2 
	8   Schulung durchführen             4/7/11          9          5 
	9   Schulung evaluieren                 8            -          1 
	10  Teilnehmer auswählen                1           11          1 
	11  Teilnehmer einladen                10            8         10 
   
   
	
	
a) Erstellen Sie ein Balkendiagramm und tragen Sie die Abhängigkeiten ein. (Bitte verwenden Sie die 
   dazu vorgesehene Vorlage auf der nächsten Seite).  
b) Erstellen Sie einen Vorgangsknotennetzplan.  
c) Führen Sie eine Vorwärts- und Rückwärtsrechnung durch und ermitteln so  
   - Projektdauer 
   - Kritischer Pfad 
   - Pufferzeiten 

   
.. raw:: latex

	\newpage 
   
Kritischer Pfad
------------------------

Als Inhaber eines Ingenieurbüros sind Sie damit beauftragt worden, die Installation einer
Rasenheizung in einem Fußballstadion durchzuführen. Bei dieser Gelegenheit soll das Stadion gleich
mit einem neuen Rasen ausgestattet werden. Dazu haben Sie bereits die folgenden notwendigen Vorgänge 
sowie ihre Abhängigkeitsbeziehungen ermittelt.


.. image:: figure/projekt/kritischer_pfad.jpg


a) Ermitteln  Sie  anhand  des  zugehörigen  MPM-Netzplanes  die  frühest  möglichen 
   und spätest möglichen Anfangs- und Endzeitpunkte für jeden Vorgang. 
   Kennzeichnen Sie den kritischen Weg ihres Projektes!  
   
   

b) Stellen Sie die in Teil a) ermittelten Zusammenhänge in einem Gantt-Chart  
   (Balkendiagramm) dar. Markieren Sie dabei die Vorgänge auf dem kritischen 
    Weg und die Pufferzeiten (alle Vorgänge starten zum frühest möglichen Termin). 


c) Nehmen Sie an, dass Ihnen zur Durchführung des Projektes 5 Mitarbeiter zur Verfügung stehen.
   Dabei nehmen die einzelnen Vorgänge die folgenden Kapazitäten in Anspruch:    

   .. image:: figure/projekt/rasenheizung_mitarbeiter.jpg

   Können Sie den Projektplan durchführen, wenn alle Vorgänge zum frühest möglichen  
   Anfangszeitpunkt beginnen  (Begründung!)? Wenn nein, welche Möglichkeiten haben Sie, 
   ohne die Aufstockung von Mitarbeitern einen zulässigen Projektplan zu erstellen, bei 
   dem das Projektende nicht verzögert wird?         
   
 
   
.. raw:: latex

	\newpage
   
Aufgabe Netzplan/GANTT
----------------------

Bearbeiten Sie die Aufgaben aus dem verlinkten Dokument.

.. sidebar:: CPM/GANTT

	:download:`CPM/GANTT <figure/projekt/Aufgabe_Netzplan_GANTT.docx>`
	
	:download:`Lösung <figure/projekt/CPM_GANTT_Loesung.pdf>`
	


