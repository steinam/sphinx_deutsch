Aufgaben
=========

Projektstart
-------------

#. Erläutern Sie die Gründe, warum in den letzten Jahrzehnten Projektmanagement an Bedeutung gewonnen hat
#. Erläutern Sie 4 charakteristische Eigenschaften von Projekten
#. Was ist mit dem magischen Dreieck gemeint?
#. Klare und einheitlich verstandene Projektziele sind wichtig für den Projekterfolg. Bitte stellen   
   Sie dar, in welcher Weise und mit welchen Methoden Sie als Projektleiter dafür sorgen, dass die Projektziele eine gute Grundlage für die weitere Planung und Durchführung bilden. 
#. In Projekten werden unterschiedliche Phasenkonzepte eingesetzt. Erläutern Sie ein allgemeines 
   Phasenkonzept. 
#. Erläutern Sie anhand von zwei Merkmalen, worin die Besonderheiten der Projektorganisation 
   gegenüber der regulären Organisation zu sehen sind.  Welche drei Grundformen der Projektorganisation lassen sich unterscheiden? 
#. Bitte benennen Sie vier Arten von Projektbeteiligten und skizzieren Sie ihre jeweiligen Aufgaben. 
#. Welche Aufgaben hat im Allgemeinen der Projektleiter? Nennen und beschreiben Sie mindestens fünf. 


Projektziele
-------------


**Lösung Engels:**

Die Firma Engels GmbH  .....

a) Projektziele bestimmen 
   Formulieren Sie bitte grob die Projektziele hinsichtlich Leistung, Kosten und Terminen. Das Leistungsziel formulieren Sie genauer als oben von Herrn Engels genannt. Die Projektdauer und die Projektkosten müssen Sie grob schützen. Denken Sie auch an die die Kosten des Einsatzes von eigenen Mitarbeitern. 

b) Erfolgskriterien festlegen 
   Nach 3 Jahren will Herr Engels den Erfolg des Projektes evaluieren. Welche Grö�en kann er dafür heranziehen? Formulieren Sie solche Messkriterien, mit der der Erfolg des Projektes bewertet werden kann. 
   
   
Zu a)

Leistungsziele 
- Projekthandbuch erarbeitet, vom Projektlenkungsausschuss gebilligt und von einem externen 

Berater auf Qualitüt geprüft 
- Schulungsunterlagen erstellt und von einem externen Berater auf Qualität geprüft 
- Projektleiter und Projektteams der Pilotprojekte sowie Leiter des Projektmanagement-Büros 
geschult 
- Projektmanagement-Software ausgewählt und beschafft 

Terminziel 
Geschätzte Dauer des Projekts 60 Arbeitswochen ab Projektfreigabe durch die Geschäftsführung 

Kostenziel 
Positionen: 
- Annahme: 
  Circa sechs Mitarbeiter des Hauses mit wöchentlich acht Stunden für die Dauer  des Projekts. Pro Woche 6 x 8 = 48 Std. pro Woche. 48 x 60 =  2.880 Mitarbeiterstunden. 
  2.880 / 8 Std. = 360 Mitarbeitertage. Wenn wir einen Tagessatz von 500 € unterstellen, kommen wir auf Kosten von 180.000 € 
- 60 Beratertage a 1000 € = 60.000 € 
- Beschaffungskosten Software geschätzt 14.000 Euro 
  Gesamtkosten: 254.000 € 
  
zu b) Erfolgskriterien 
Der Anwendungserfolg wird drei Jahre nach dem Start der Pilotprojekte gemessen. Vergleichsgrö�e 
für die Zielwerte sind die Werte (Vergleichswerte) zum Stichtag (Start der Pilotprojekte). Diese 
Vergleichswerte werden vom Controlling zur Verfügung gestellt und von der Qualitätssicherung 
bestätigt. 

Terminziel 
Die Auswertung abgeschlossener Projekte ergab eine durchschnittliche Terminüberschreitung um 40 
Prozent bezogen auf den im Projektantrag angegebenen Starttermin des Projekts. Dieser Wert soll 
durch Projektmanagement auf durchschnittlich 15 Prozent gesenkt werden. 

Kostenziel 
Die durchschnittliche Überschreitung der Entwicklungskosten (bezogen auf die im Projektantrag 
angegebenen Entwicklungskosten) von 35 Prozent soll auf 20 Prozent reduziert werden. Die 
Fertigungskosten der Produkte sind um zehn Prozent zu verringern. 

Qualitätsziel 
Der Anteil der Geräte, die Fachhändler innerhalb der Garantiefrist wegen Qualitätsmängeln 
zurückschicken, muss von sechs auf drei Prozent reduziert werden.

Projektdeckungsbeitrag 
Der durchschnittliche Projektdeckungsbeitrag in Höhe von 40 Millionen Euro soll preisbereinigt um 
zehn Prozent erhöht werden.

Zufriedenheit der Stakeholder 
90 Prozent der ausgewählten Kunden müssen die Qualität der Produkte mindestens mit der Note 
"gut" bewertet haben (Schulnoten). Ein Vergleichswert aus früheren Projekten liegt nicht vor. 60 
Prozent der befragten Mitarbeiter müssen das eingeführte Projektmanagement-System mindestens 
mit der Note "gut" (d.h. Verbesserung der Projektabwicklung gegenüber vorher) bewerten. Weitere 
20 Prozent müssen es mindestens mit der Note "sehr gut" bewerten (d.h. erhebliche Verbesserung 
gegenüber vorher). 

Projektstrukturplan
--------------------

Lösung:

.. image:: figure/projekt/loesung_psp.jpg

.. image:: figure/projekt/loesung_vorgangsliste.jpg



Lösung Kritischer Pfad
------------------------

Als Inhaber eines Ingenieurbüros sind Sie damit beauftragt worden, die Installation 
einer Rasenheizung in einem Fu�ballstadion durchzuführen. Bei dieser Gelegenheit 
soll das Stadion gleich mit einem neuen Rasen ausgestattet werden. Dazu haben Sie 
bereits die folgenden notwendigen Vorgänge sowie ihre Abhängigkeitsbeziehungen 
ermittelt: 

.. image:: figure/projekt/kritischer_pfad.jpg

a) Ermitteln  Sie  anhand  des  zugehörigen  MPM-Netzplanes  die  frühest  möglichen 
   und  spätest  möglichen  Anfangs-  und  Endzeitpunkte  für  jeden  Vorgang. 
   Kennzeichnen Sie den kritischen Weg ihres Projektes!  


.. sidebar:: Lösung MS Projekt
	
	:download:`L_Rasenheizung <figure/projekt/projekt_rasenheizung_loesung.pdf>`
	

.. image:: figure/projekt/loesung_kritischer_Weg.jpg



b) Stellen Sie die in Teil a) ermittelten Zusammenhänge in einem Gantt-Chart  
   (Balkendiagramm) dar. Markieren Sie dabei die Vorgänge auf dem kritischen 
    Weg und die Pufferzeiten (alle Vorgänge starten zum frühest möglichen Termin). 
    
.. sidebar:: Lösung MS Projekt

	:download:`L_Rasenheizung <figure/projekt/loes_msproj_rasenheizung_gantt.jpg>`
    
c) Nehmen Sie an, dass Ihnen zur Durchführung des Projektes 5 Mitarbeiter zur Verfügung stehen.
   Dabei nehmen die einzelnen Vorgänge die folgenden Kapazitäten in Anspruch:

   .. image:: figure/projekt/rasenheizung_mitarbeiter.jpg

    Können Sie den Projektplan durchführen, wenn alle Vorgänge zum frühest möglichen  
    Anfangszeitpunkt beginnen  (Begründung!)? Wenn nein, welche Möglichkeiten haben Sie, 
    ohne die Aufstockung von Mitarbeitern einen zulässigen Projektplan zu erstellen, bei 
    dem das Projektende nicht verzögert wird?   
    
    
    Wenn alle Vorgänge zum frühest möglichen Zeitpunkt gestartet werden reichen 
    die  5  Mitarbeiter  nicht  aus,  um  das  Projekt  durchzuführen.  Dies  ist  in  dem 
    folgenden Diagramm zu sehen: 

    .. image:: figure/projekt/loesung_rasenheizung_mitarbeiter.jpg
    
    
    Um den Ressourcenkonflikt zu lösen, kann man Vorgang E um 4 Zeiteinheiten nach hinten verschieben. Dieser beginnt dann zum Zeitpunkt 23. Es ergibt sich das folgende Gantt 
    Diagramm.  
    
    .. image:: figure/projekt/loesung_rasenheizung_mitarbeiter_ohne_konflikt.jpg

    
    
    
    

