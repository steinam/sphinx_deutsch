Excel
======


Zahlen visualisieren - Grundlagen
----------------------------------

Wissenschaftlicher Ansatz
	Expressivität
	Geschichte der Geschäftsdiagramme
	
	
Visualisierung - Thesen, Methoden, Werkzeuge
	Das Pyramidenprinzip
	Geschäftsgrafik nach Tufte
	Wie aus Zahlen Bilder werden - Gene Zelazny
	

	
Beispiele zur Visualisierung mit Excel
---------------------------------------



Andere Diagramme
^^^^^^^^^^^^^^^^^^^^^^^^^^^^


GANT-Diagramm
^^^^^^^^^^^^^^





Alternativen zu Excel
--------------------------

Arbeiten mit graphviz, pgf/tikz, processing
Dies in Form von Referaten und Vorträgen




