Projektmanagement
=================


Projektmanagement hat eine lange Geschichte. Schon der Bau der Pyramiden, des Suezkanals und des Kölner Doms waren komplexe Aufgaben, die ein hohes Maß an Planung und Organisation erforderten. Die direkten Ursprünge des heutigen Projektmanagements liegen im 2. Weltkrieg, in dem US-Militärs Methoden zur Steuerung des massenhaften Einsatzes von Menschen und Material entwickelten. Das größte militärische Forschungsprojekt dieser Art war das Manhattanprojekt, in dem die USA die Atombombe bauten. Diese Planungs- und Managementmethoden wurden vom Militär und der Raumfahrt (z.B. beim Apollo-Programm der NASA) weiterentwickelt. In den 60er Jahren wurden sie schließlich auf die Industrie übertragen und finden inzwischen auf breiter Ebene auch in kleinen und mittleren Unternehmen Anwendung. Viele Aufgaben in den Unternehmen werden immer häufiger als Projekte geplant und realisiert.

Projekte sind also nichts Neues. Sie wurden schon früher durchgeführt, ohne dass allerdings dieser Begriff verwendet worden wäre. Doch damals musste man sich bei den organisatorischen und planerischen Aufgaben auf den gesunden Menschenverstand und handgestrickte Methoden verlassen. Heute stellt jedoch das Projektmanagement dafür ein umfangreiches Repertoire an ausgereiften Methoden und Instrumenten zur Verfügung, die die Projektarbeit wesentlich erleichtern und bessere Projektergebnis ermöglichen.




Eigenschaften
---------------

Projekte haben bestimmte Merkmale, anhand derer geprüft werden kann, ob die Bezeichnung "Projekt" im Einzelfall gerechtfertigt ist. Ein Projekt liegt nur dann vor, wenn es die nachstehenden Eigenschaften aufweist:



.. only:: html

	- Einmaligkeit
	- Definiertes Ziel bzw. Zielvorgabe
	- Zeitliche, finanzielle und personelle Rahmenbedingungen
	- Abgrenzung zu anderen Vorhaben
	- Projektspezifische Organisation
	- Mindestmaß an Komplexität

.. raw:: latex

	\newpage
	
.. only:: latex

	- 
	
        -
        
        -
        
        -
        
        -
        
        -
        


**Projektphasen und ihre Aufgaben**


Projekte müssen einen definierten Anfang und Abschluss haben. Sie lassen sich entsprechend Eigenschaft und Rahmenbedingungen nach vier Phasen gliedern.



1) Vorbereitung oder Projektstart 
2) Planung 
3) Durchführung und 
4) Abschluss


.. only:: latex

	.. image:: figure/projekt/projekt_phase_schueler.jpg


.. only:: html

	.. image:: figure/projekt/projekt_phase.jpg


	.. sidebar:: Oder doch eher so
	
		.. image:: figure/projekt/projekt_phase_karikatur.jpg
		
		.. image:: figure/projekt/projekt_phase_schueler.jpg


Beginn und Ende jeder Phase sind in der obigen Grafik durch Meilensteine definiert.

Für das Projektmanagement (gemeint ist hier der Projektleiter oder der Projektleiterin), stellen sich sowohl Aufgaben, die nur, oder vorwiegend, in bestimmten Projektphasen anfallen, als auch solche, die sich über den ganzen Projektlebenszyklus hinwegziehen. Dazu gehören die Führung des Teams, die Koordination sowohl nach innen, d.h. gegenüber den Projektmitarbeitern, als auch gegenüber außen, beispielsweise gegenüber der Geschäfts- / Abteilungsleitung oder gegenüber dem Kunden.


Projektstart
--------------

.. only:: latex

	.. image:: figure/projekt/projekt_start_schueler.png	


.. only:: html

	.. image:: figure/projekt/projekt_start.jpg

	
	
Bereits in der Projektvorbereitung werden die entscheidenden Weichen über Erfolg oder Misserfolg eines Projektes gestellt. Unklare Projektziele, mangelnde Einbindung der Anwender und der Kunden sowie unklare Verteilung von Aufgaben und Verantwortung tragen zum Scheitern von Projekten bei. Eine sorgfältige Vorbereitung in der Projektstartphase kann das verhindern.

Für den Begriff "Projektstart" finden sich zwei ganz unterschiedliche Definitionen:

- Projektstart als ein Ereignis. 
  Das könnte etwa der Zeitpunkt sein, an dem eine Idee für ein internes Projekt dem Entscheidungsgremium vorgelegt wird. Beim Kundenprojekt das Datum, zu dem eine Anfrage für die Erstellung eines Angebotes eingeht, oder der Tag der Vertragsunterzeichnung.

- Projektstart als eine Phase. 
  Der Projektstart ist eine eigenständige Projektphase mit einer gewissen Dauer, die mit dem Kickoff-Meeting endet. Wir werden hier die zweite Definition verwenden: Projektstart ist damit die Phase, in der das Projekt vorbereitet wird (auch bezeichnet als Projektinitialisierung).


Aus dem Gefühl von Zeitdruck heraus werden Projekte oft schlampig vorbereitet:

    Projektziele werden nicht klar definiert.
    Die Stakeholder werden nicht mit einbezogen.
    Zuständigkeiten und Verantwortungen werden nicht klar festgelegt.
    Unterschiedliche Vorstellungen über die Vorgehensweise und die eigenen Aufgaben werden nicht ausdiskutiert.
    Es wird schlampig geplant und der Aufwand zu optimistisch geschätzt
    Risiken werden ausgeblendet ("Wird schon gut gehen").
    Die Dokumentation wird überhaupt nicht oder nur mangelhaft geplant und organisiert.
    Interne und externe Kommunikation werden nicht abgestimmt. Es wird nicht geklärt, wie die Dokumentation und das Berichtswesen gemacht werden sollen.



    
**Projektziele**
~~~~~~~~~~~~~~~~~

Im Projektstart müssen das Projektziel oder die -ziele bestimmt werden. Soll ein neues Produkt entwickelt werden, muss Klarheit darüber gewonnen werden, wie dieses aussehen, was es kosten soll, etc. Geht es um einen Kundenauftrag, muss gemeinsam mit dem Kunden geklärt werden, wie das Projektergebnis aussehen soll.


.. only:: latex

	Klare Projektziele
	
	- 
	
	-
	
	-
	
	-
	
	- 




.. only:: html


	Klare Projektziele
	
	- bilden die Grundlage einer guten Planung
	- ermöglichen ein ergebnisorientiertes Arbeiten
	- dienen zur Korrektur falscher Erwartungen, z. B. beim Auftraggeber
	- bilden die Basis für ein gut funktionierendes Projektcontrolling
	- dienen als Gradmesser für Erfolg oder Misserfolg des Projektes



Eigenartigerweise stellt die mangelnde Zielpräzisierung in der Praxis häufig ein Problem dar. Auftraggeber und Auftragnehmer besprechen das Projekt und gehen davon aus, dass sie sich schon verstanden haben. Dies ist aber nicht immer der Fall. Nützlich, und bei bestimmten Projekten sogar unabdingbar, ist eine detaillierte schriftliche Festlegung des angestrebten Projektergebnisses.

Wichtig ist die Unterscheidung zwischen Projektergebnis und Projektzweck. Beispielsweise sollen in einem Trainingsprojekt 10 Mitarbeiter fünf Tage die Woche in einem Software-Programm geschult werden. Wenn am Freitagabend alle das Programm bedienen können, ist das angestrebte Projektergebnis erreicht. Doch ob der eigentliche Zweck, nämlich ob mit der neuen Software die Kundenverwaltung verbessert wird, steht noch nicht fest. Das hängt von anderen Bedingungen ab. Im Englischen bringt man diesen Unterschied mit den Begriffen **Output** (das Ergebnis) und **Outcome** (Zweck) zum Ausdruck.


**Zielformulierungsregeln S.M.A.R.T**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

SMART steht als Abkürzung für "Specific Measurable Accepted Realistic Timely" und dient zur Formulierung von Zielen. Ein Ziel sollte allen der folgenden Kriterien genügen:



    
.. only:: html

       - Spezifisch-konkret (präzise und eindeutig formuliert)
       - Messbar (quantitativ oder qualitativ)
       - Attraktiv (positiv formuliert, motivierend)
       - Realistisch (das Ziel muss erreichbar sein)
       - Terminiert (bis wann…?)
 

       .. sidebar:: AB Zieldefinition
	
		:download:`AB_Projektziele_loesung <figure/projekt/AB_Projektziele_inc_Loesung.docx>`
		
		:download:`AB_Projektziele <figure/projekt/AB_Projektziele.docx>`
	
		
    
Da Ziele oft in wechselseitiger Abhängigkeit zueinander stehen, sind diese Beziehungen bei der Zielformulierung zu berücksichtigen. Insgesamt lassen sich vier verschiedene Arten von Zielbeziehungen unterscheiden:


.. image:: figure/projekt/projekt_start_zielantimonie.jpg




.. only:: html

    a) Zielantinomie: Zwei Ziele lassen sich gleichzeitig nicht erreichen.
    b) Zielkonkurrenz: Die Mehrerfüllung des einen Ziels führt zu Einbußen bei einem anderen Ziel.
    c) Zielneutralität: Die beiden Ziele stehen in keinem Zusammenhang.
    d) Zielkomplementarität: Eine höhere Zielerreichung beim einen Ziel führt auch zu einer höheren Zielerreichung bei einem anderen Ziel.


.. only:: latex

    - (a): 
    
    - (b): 
    
    - (c):
    
    - (d): 
    
    

.. image:: figure/projekt/projekt_start_dreieck.jpg


**Projektphasen**
------------------

In der Phase des Projektstarts ist in der Regel noch keine eingehendere Planung möglich. Aber es lässt sich eine Grobplanung machen. Ein erster Schritt dazu ist der Entwurf eines Phasenkonzepts, das festlegt, in welchen zeitlich aufeinanderfolgenden Phasen das Projekt ablaufen soll. Die meisten Projektmanagement-Lehrbücher gehen von einem Vier-Phasen-Konzept aus:

.. admonition:: Projektphase

	Zeitlicher Abschnitt eines Projektablaufs, der sachlich gegenüber anderen Abschnitten getrennt ist. (DIN 69 901)
	
	



.. image:: figure/projekt/projekt_phase_allgemein.jpg

Am Anfang steht die Projektvorbereitung oder der Projektstart, dann kommt die Planungsphase, dann die Durchführungsphase. Am Schluss steht die kurze, aber wichtige Phase des Projektabschlusses. In allen vier Phasen kommen auf den Projektleiter und die anderen Projektbeteiligten ganz spezifische Aufgaben zu. Im Zuge einer Grobplanung in der Projektstartphase ist es wichtig, sich erst einmal über die Phasen klar zu werden.



Da die Art der Phasengliederung des Projektablaufs wesentlich von der Projektart, Größe und Komplexität bestimmt ist, gibt es kein allgemein gültiges Phasenmodell (manchmal auch Projekt-Lebenszyklusmodell genannt). Für jedes Projekt sollte daher als erster Planungsschritt ein geeignetes Phasenschema maßgeschneidert werden.


.. only:: html

	.. sidebar:: Aufgabe
	
		Informieren Sie sich über verschiedene Phasenmodelle, insbesondere auch im Bereich der Softwareentwicklung.


.. only:: latex

	.. admonition:: Aufgabe
	
		Informieren Sie sich über verschiedene Phasenmodelle, insbesondere auch im Bereich der Softwareentwicklung.
		
		

Am Phasenmodell wird oft kritisiert, dass es die Projektrealität nicht spiegelt, denn:

- in der Durchführung gibt es immer wieder Rücksprünge in frühere Phasen
- die einzelnen Phasen sind nicht klar voneinander getrennt
- die Aktivitäten der einzelnen Phasen laufen oft zeitlich parallel.

.. raw:: latex

	\newpage
	

**Projektorganisation**
-----------------------

Es lassen sich drei Grundformen der Projektorganisation unterscheiden 

- Die "reine" Projektorganisation
- Die Stabs-Projektorganisation
- Die Matrix-Projektorganisation

Die drei Grundtypen unterscheiden sich insbesondere hinsichtlich

- der Funktion und dem Maß an Entscheidungskompetenz des Projektleiters
- dem Grad der Einbindung der Teammitglieder in das Projekt
- der Eigenständigkeit des Projekts und seinem Projektcharakter.

.. only:: latex

	.. admonition:: Arbeitsblatt
		
		Ab_Projektorganisation.doc
		
		
	.. image:: figure/projekt/projekt_organisation_grundformen_schueler.jpg	

.. only:: html

	.. sidebar:: Arbeitsblatt
	
		:download:`AB_Projektorg <figure/projekt/AB_Projektorganisation_incl_Loesung.docx>`


	.. image:: figure/projekt/projekt_organisation_grundformen.jpg

	
	
.. raw:: latex

	\newpage

**Projektleiter und Team**
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. image:: figure/projekt/projektteam.jpg


Der Projektleiter trägt die Verantwortung für das Gesamtprojekt. Um das Projekt zum Erfolg zu führen, muss er eine Reihe von Aufgaben wahrnehmen. Dafür sollte er die notwendigen Entscheidungs- und Weisungskompetenzen im Rahmen des Projektauftrages besitzen. Für seine Aufgabe benötigt er ferner spezifische Fähigkeiten.


	.. admonition:: Arbeitsblatt
	
		AB_Projektleiter_Team.docx
		


.. only:: html

	.. sidebar:: AB PLtr/Team
	
		:download:`Arbeitsblatt <figure/projekt/AB_Projektleiter_Team.docx>`
		



**Aufgaben des Projektleiters**

.. only:: latex

	-
	
	-
	
	-
	
	-


.. only:: html

	- Planung des Projekts
	- Teamführung und Einsatzplanung der Mitarbeiter
	- Projektüberwachung und Steuerung
	- Kommunikation nach Innen und Außen

**Verantwortung des Projektleiters**

.. only:: latex

	-
	
	-
	
	-
	
	-


.. only:: html

	- Erreichen der Projektziele
	- Einhaltung des Budgets
	- Vertragserfüllung
	- Berichterstattung

**Kompetenzen des Projektleiters i.S. von Befugnissen**

.. only:: latex

	- 
	
	- 
	
	- 
	
	- 


.. only:: html

	- Für die Erfüllung seiner Aufgabe bekommt er üblicherweise die folgenden Aufgaben und Entscheidungskompetenzen zugeteilt:
	- Projektentscheidungen treffen
	- Auswahl, Planung und Einsatz der Mitarbeiter
	- Eventuell disziplinarische Kompetenz

Der Projektleiter benötigt diese Entscheidungskompetenzen, damit er die Verantwortung auch wahrnehmen kann.

Eine andere Art der Klassifizierung von Kompetenzen könnte folgende Struktur sein:

**Kompetenzen i.S. von Fähigkeiten**

- Fachkompetenz (fachliche Qualifikation und Erfahrung)
- Methodenkompetenz (Führung, Projektmanagement)
- Sozialkompetenz (mit Menschen umgehen können)
- Persönliche Kompetenz (Durchhaltevermögen, Durchsetzungsfähigkeit, Frustrationstoleranz, etc.)

Der Projektleiter braucht beides, sowohl Kompetenzen im Sinne von Entscheidungsbefugnissen, die ihm für seine Projektarbeit gewährt werden, als auch Kompetenzen im Sinne von Fähigkeiten. Er muss nicht nur vom Fach was verstehen, sondern braucht Fähigkeiten hinsichtlich Führung, insbesondere Teamführung, und die sogenannten soft skills. Und natürlich braucht er umfassende Kenntnisse in Projektmanagement.


.. raw:: latex

	\newpage
	
	

**Das Team**
~~~~~~~~~~~~~

Das Projektteam setzt sich meist aus folgenden Arten von Mitarbeitern zusammen:

Die Kernmitglieder sind während der gesamten Projektdauer mit dabei. Entweder sind sie voll für das Projekt abgestellt oder sie arbeiten nebenbei im Projekt mit. Die zeitweiligen Mitglieder arbeiten nur in bestimmten Phasen mit. Eventuell kommen Mitarbeiter von externen Dienstleistern und Zulieferern dazu.

Der Projektleiter stellt üblicherweise das Projektteam gemeinsam mit dem Lenkungsausschuss zusammen. Es sind Personen auszuwählen, die von ihrer Abteilungszugehörigkeit und ihrer Fachkompetenz her einen wesentlichen Beitrag für den Projekterfolg leisten können. Sollten noch spezifische Kenntnisse fehlen, können diese eventuell noch durch entsprechende Fortbildungsmaßnahmen erworben werden.

Das Projektteam sollte aus Mitgliedern bestehen, die sich sowohl fachlich als auch persönlich gut ergänzen. Man benötigt Spezialisten für die einzelnen Aufgaben. Wenn Mitarbeiter aus anderen Abteilungen benötigt werden, muss deren zeitliche Verfügbarkeit mit den jeweiligen Abteilungsleitern abgestimmt werden. Innerhalb des Teams sind die Aufgaben und Zuständigkeiten der Teammitglieder zu verteilen. Dazu dient der Projektstrukturplan (PSP), mit dessen Hilfe sich Arbeitspakete definieren und den einzelnen Projektmitarbeitern eindeutig zuordnen lassen.



In der ersten Euphorie und Hektik zu Projektbeginn werden häufig die Aufgaben, Verantwortlichkeiten und Entscheidungskompetenzen der Projektmitarbeiter nicht ausreichend festgelegt. Man geht davon aus, dass sich das schon irgendwie finden wird. Dieser Optimismus ist nicht immer berechtigt: In vielen Projekten kommt es in späteren Projektphasen zu Konflikten, weil Aufgaben und Verantwortungen nicht besprochen und festgelegt wurden. Deshalb sollten diese Dinge beispielsweise im Projektstart-Workshop geklärt werden.

Projektleiter berücksichtigen die menschliche Seite oft nur unzureichend. Die Teammitglieder sollten sowohl persönlich gut miteinander klar kommen als sich auch in ihren fachlichen Kompetenzen zu einem harmonischen und leistungsstarken Team ergänzen. Wann immer möglich, sollten bei der Teamzusammenstellung auch diese sozialen und persönlichen Aspekte berücksichtigt werden.


**Partizipativ-kooperativer Führungsstil**

Der autoritäre Führungsstil ist für die Projektarbeit in der Regel ungeeignet. In der Rolle eines Projektleiters übernehmen Sie besser einen partizipativ-kooperativen Führungsstil mit den folgenden Charakteristiken:

- Treffen Sie Entscheidungen partizipativ. Zeigen Sie ihren Mitarbeitern, dass Sie sie als Experten akzeptieren.
- Delegieren Sie die Aufgaben und die Verantwortung für die einzelnen Arbeitspakete. Denken Sie daran, dass Sie dem Mitarbeiter auch die entsprechenden Befugnisse und Kompetenzen einräumen.
- Geben Sie Ihren Mitarbeitern Gestaltungs- und Handlungsfreiräume, in denen sie frei entscheiden können.
- Äußern Sie Lob und Anerkennung.
- Pflegen Sie eine offene und vertrauensvolle Kommunikation. Informieren Sie Ihre Mitarbeiter umfassend und schnell.
- Sorgen Sie für eine freundliche Atmosphäre. Persönliche Verletzungen oder unsachliche Kritik wirken demotivierend.
- Lösen Sie eventuell im Team auftretende Konflikte gemeinsam mit Ihren Mitarbeitern.



.. raw:: latex

	\newpage

Projektplanung
----------------

Schon die Vorbereitung während des Projektstarts umfasste auch planerische Aufgaben. Es wurde bereits eine Grobplanung gemacht und das Projekt in Phasen gegliedert. Aber die eigentliche Planungsphase kommt jetzt. Jetzt geht es darum, das Vorhaben im Detail zu planen. Man spricht daher auch von Detailplanung. Diese Planung legt die Basis für die gesamte Abwicklung des Projekts und ist deshalb das Herzstück des Projektmanagements.



- Phasen- und Meilensteinplan
- PSP, Arbeitspakete, Aufwandsschätzung

.. image:: figure/projekt/projektplanung.jpg




Projektstrukturplan (PSP) und Arbeitspakete
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


Die Basis für die Planung ist der Projektauftrag, für den je nach Branche und Unternehmen auch andere Begriffe verwendet werden: Leistungsbeschreibung, Pflichtenheft, Lastenheft, Anforderungsdokument oder Spezifikation. Der Projektauftrag beschreibt als ein schriftliches Dokument das gewünschte Projektergebnis, d.h. die Eigenschaften und Funktionen, die ein Produkt, eine Dienstleistung oder ein Ergebnis kennzeichnen.


.. only:: html

	.. admonition:: Arbeitsblatt
	
		AB_Projektstrukturplan.docx

.. only:: html

	.. sidebar:: Arbeitsblatt
	
		:download:`AB_PSP <figure/projekt/AB_Projektstrukturplan.docx>`


.. image:: figure/projekt/projektarbeiten.jpg

Die Projektplanung beginnt mit der Erarbeitung des Projektstrukturplans, oder wie man kurz sagt, des "PSP". Da im Zuge der Internationalisierung auch immer mehr englische Begriffe in die deutsche Projektmanagement-Sprache einwandern, hört man immer mehr den entsprechenden englischen Begriff, nämlich Work Breakdown Structure (WBS).

Ausgehend von einem klar definierten Projektauftrag (z.B. im Pflichtenheft festgelegt) werden bei der Erstellung des PSP sämtliche zur Erreichung des Projektziels durchzuführender Arbeitspakete ermittelt. Dazu ist meist hilfreich, die Gesamtaufgabe in einem Zwischenschritt zuerst in Teilprojekte zu gliedern. Üblicherweise wird der PSP grafisch als hierarchische Baumstruktur dargestellt.

.. image:: figure/projekt/projektstrukturplan.jpg

Ziel des PSP ist die inhaltliche Untergliederung der Gesamtaufgabe, aber noch nicht die Ermittlung der zeitlichen Reihenfolge der einzelnen Aktivitäten. Auf der untersten Ebene des PSP werden alle Aktivitäten zu Arbeitspaketen (AP) zusammen gefasst und genau einem Verantwortlichen zugeordnet. Nach DIN 69 901 ist das Arbeitspaket das "kleinste, nicht weiter zergliederte Element im Projektstrukturplan, das auf einer beliebigen Projektstrukturebene liegen kann".

Hauptaufgabe des PSP ist die Ermittlung sämtlicher zur Erreichung des Projektziels durchzuführender Arbeitspakete. Doch er erfüllt noch weitere Zwecke  Der PSP

- ist Grundlage für die weiteren Planungsschritte (Ablauf, Termine, Ressourcen, Kosten)
- ist Voraussetzung für die dazu notwendigen Schätzungen
- dient der Verteilung der Aufgaben und Verantwortlichkeiten
- ist Grundlage für die Strukturierung der Dokumentation und der Berichte
- unterstützt das Risikomanagement
- hilft zur Strukturierung der Projektsitzungen und bei der
- Vergabe von Projektcode-Nummern

.. image:: figure/projekt/beispiel_psp.jpg

**Arbeitspakete** sind die Grundbausteine für die Projektplanung. Sie sind für die Arbeitsverteilung an die Mitarbeiter notwendig. Für jedes Arbeitspaket sind die zu erarbeitenden Ergebnisse und die dazu notwendigen Tätigkeiten (Aktivitäten) detailliert zu beschreiben. Für jedes Arbeitspaket ist ein Verantwortlicher zu benennen. Gut formulierte Arbeitspakete sind auch für das Projekt-Controlling in der Durchführungsphase unerlässlich.

.. admonition:: Arbeitspaket 

	"Teil des Projektes, der im Projektstrukturplan nicht weiter aufgegliedert ist und auf einer beliebigen Gliederungsebene liegen kann." (DIN 69 901)

   
Für unsere Zwecke gehen wir davon aus, dass ein Arbeitspaket identisch ist mit einem Vorgang (Arbeitspaket = Vorgang). Beide bestehen jeweils aus einer Anzahl von Tätigkeiten oder Aktivitäten.


**Wie groß sollen Arbeitspakete sein?**

Bei der Erarbeitung des PSP stellt sich die Frage nach dem adäquaten Detaillierungsgrad. Einfacher gesagt: Wie groß soll man die einzelnen Arbeitspakete machen? Zweierlei ist dabei zu bedenken:

    Eine geringe Detaillierung, d.h. sehr große Arbeitspakete, macht es später bei der Durchführung schwierig, Abweichungen frühzeitig zu erkennen und so noch gegensteuern zu können.
    Eine zu hohe Detaillierung, d.h. sehr kleine Arbeitspakete, bedeutet schon bei der Planung einen erheblichen Aufwand. Aber auch das Controlling wird aufwendig, wenn für jedes einzelne AP der Leistungsfortschritt und die Kostenentwicklung verfolgt werden muss.

Die Dauer eines Arbeitspakets sollte – verglichen mit der Projektdauer – nicht zu groß sein, da sonst ein eventueller Terminverzug zu spät erkannt wird. Der Kostenplanwert für ein Arbeitspaket darf nicht zu klein sein, weil sonst die Kostenkontrolle zu schwerfällig wird. Andererseits darf er nicht zu groß sein, weil dann die Kostensteuerung schwierig wird. Richtwert: 1 bis 5 Prozent der Gesamtkosten [Schelle, 2010, S. 130].



Arbeitspakete sollten

- klar hinsichtlich ihrer Ziele, Aktivitäten, Kosten und Zeiten beschrieben sein
- sie sollten als eigenständige, von anderen Arbeitspaketen klar abgegrenzte Einheiten formuliert sein
- jeweils bestimmten Stellen oder Personen zugeordnet werden können
- für jedes Arbeitspaket sollte es nur einen Verantwortlichen geben
- Aufgaben, die nach außen vergeben werden, sind als eigenes Arbeitspaket auszuweisen 


.. image:: figure/projekt/arbeitspaket.jpg






Ablaufplanung
~~~~~~~~~~~~~~~~

Ein in sich stimmiger und vollständiger PSP ist Grundlage für die folgenden Planungsschritte. Der PSP sagt uns, welche Arbeitspakete durchzuführen sind, um das Projektergebnis zu erreichen. Aber wir wissen noch nicht, in welcher Reihenfolge die Arbeiten (die Arbeitspakete oder Vorgänge) durchzuführen sind. Dazu bedarf es der Ablaufplanung, in der die logisch-zeitliche Abfolge der Projektaktivitäten ermittelt bzw. festgelegt wird. Dazu ist zu klären:

- Welche Vorgänge müssen abgeschlossen sein, damit ein Vorgang begonnen werden kann?
- Ist ein Vorgang seinerseits Voraussetzung für andere Vorgänge?
- Welche Vorgänge können nur zeitlich nacheinander, welche auch parallel durchgeführt werden?


.. image:: figure/projekt/vorgangsliste.jpg





Zeit- und Terminplanung
------------------------

Die Terminplanung baut auf dem Ablaufplan auf. Die Dauern der einzelnen Vorgänge müssen geschätzt werden. Daraus lassen sich festlegen:

- Dauer sowie Anfang und Ende der Vorgänge
- Pufferzeiten
- Die gesamte Projektdauer

Es gibt drei Methoden der Terminplanung:

- die Listungstechnik
- das Balkendiagramm (GANTT)
- die Netzplantechnik

In der Praxis kommt vor allem das Balkendiagramm, und bei umfangreicheren Projekten die Netzplantechnik zur Anwendung. Netzpläne werden aber in der Praxis selten handschriftlich gezeichnet. Heute werden für derartige Planungsaufgaben meist Projektmanagement-Software eingesetzt, wofür zahlreiche Programme zur Verfügung stehen. Das Spektrum reicht von einfachen und kostenlosen Open-Source-Lösungen, bis hin zu kostenspieligen branchenspezifischen Lösungen.

Welche Methode sinnvollerweise einzusetzen ist, hängt vom Projekt, insbesondere seiner Größe und Komplexität, ab. So wird es sich kaum lohnen, für ein Projekt mit fünf Arbeitspaketen einen Netzplan zu erarbeiten. Man kann es natürlich trotzdem machen, zumal wenn man eine Projektmanagement-Software zur Verfügung hat und die man gut beherrscht. Aber für so einen Fall wird ein einfaches Balkendiagramm auf einem Karo-Papier reichen. Also: Nicht mit Kanonen auf Spatzen schießen, sondern die jeweils passende Methode für das Projekt wählen.

GANTT-Diagramm
~~~~~~~~~~~~~~~

Das Balkendiagramm (manchmal auch Gantt-Diagramm genannt, nach Henry L. Gantt, 1861–1919) ist eine relativ einfache und anschauliche Form, den zeitlichen Ablauf der einzelnen Arbeitspakete eines Projektes als Balken darzustellen. Es eignet sich auch für die Terminkontrolle in der Durchführungsphase. Für einfache Projekte mit wenigen Vorgängen reicht das Balkendiagramm für die Terminplanung völlig aus. Der Vorteil dieser Art der Darstellung ist eine gute Übersichtlichkeit.

Für das Projekt „Automatisierung Presse“ sieht der PSP und die dazugehörige Terminplanung als Balkendiagramm so aus:


.. image:: figure/projekt/psp.jpg
.. image:: figure/projekt/gantt.jpg


Meilensteine
~~~~~~~~~~~~~

Zumindest am Ende jeder Projektphase sollte ein Meilenstein gesetzt werden. Ein Meilenstein ist ein klar definiertes Teilergebnis, das zu einem bestimmten Termin erreicht sein muss.

.. admonition::

	Nach DIN 69 900 ist ein Meilenstein "ein Ereignis besonderer Bedeutung".

Das Projekt beginnt mit dem Meilenstein "Start". Am Ende jeder Phase steht ebenfalls ein Meilenstein (z.B. "Projektabschnitt 1 fertig" ist als Ereignis eingetreten). Jedem Meilenstein werden geplante Projektergebnisse (Meilenstein-Inhalte) und ein Plantermin (Meilenstein-Termin) zugeordnet. Die Projektergebnisse werden eindeutig und messbar definiert. Nur so lässt sich eindeutig feststellen, ob sie eingetreten sind oder nicht. Meilenstein lassen sich in den Meilenstein-Netzplan und in das Balkendiagramm einzeichnen und so visualisieren.


Netzplantechnik
~~~~~~~~~~~~~~~~

Die Netzplantechnik beschreibt die zeitliche und logischeVerkettung von Aktionen. Sie findet ihre Anwendung insbesondere in der Terminplanung von Projekten. Die Grundlage für einen Netzplan ist häufig ein Projektstrukturplan buw. eine Vorgangsliste.

Der Einsatz der Netzplantechnik soll vier wichtige Fragen beantworten:

- **Wie lange** wird das ganze Projekt dauern? Welche Risiken treten dabei auf?
- Welche **kritischen Aktivitäten** können das gesamte Projekt verzögern, wenn sie nicht rechtzeitig fertig werden?
- Ist das Projekt im Zeitplan, wird es **früher oder später** fertig?
- Wenn es früher fertig werden soll, was ist am besten zu tun, wie kann eine Beschleunigung mit den geringsten Kosten erreicht werden?

Das Konzept der Netzplantechnik beruht auf der Erfahrung, dass wenige Aktivitäten, die den längsten Pfad durch das Netzwerk bilden, den Verlauf des gesamten Projektes bestimmen. Wenn diese kritischen Aktivitäten (=kritischer Pfad) frühzeitig erkannt werden, können frühzeitig Gegenmaßnahmen ergriffen werden. Das Management kann sich auf die kritischen Aktivitäten konzentrieren. Unkritische Aktivitäten können umgeplant werden, ohne das gesamte Projekt zu beeinflussen.

Bei der Netzplantechnik unterscheidet man entsprechend vier Teilaufgaben:

- Zeitplanung: Vorgängen werden Zeitwerte zugeordnet
- Kapazitätsplanung: Planung der erforderlichen Produktionsmittel
- Kostenplanung
- Strukturplanung: Darstellung der logischen Zusammenhänge

Im Kontext der Netzplantechnik sind folgende Begriffe von Bedeutung.

**Vorgang**

.. only:: html

	.. sidebar:: Infos
	
		.. image:: figure/projekt/netzplan_beispiel.jpg
	
	
Ein Vorgang ist im Rahmen der Netzplantechnik eine abgegrenzte Arbeitseinheit, die zu einem bestimmten Zeitpunkt begonnen und einem bestimmten späteren Zeitpunkt beendet wird. Ein solcher Vorgang besitzt eine wesentliche Eigenschaft, seine Dauer. Aufgabe der Netzplantechnik ist, unter Berücksichtigung der Dauer der einzelnen Vorgänge und unter Berücksichtigung ihrer Abhängigkeiten zu ermitteln, wann die jeweiligen Vorgänge stattfinden. Der Rechenprozess beginnt je nach Bedarf entweder bei den Startvorgängen, und setzt von diesen ausgehend den frühestmöglichen Starttermin der nachfolgenden Vorgänge fest (Vorwärtsplanung), oder bei den letzten Vorgängen des Netzes (die keinen Nachfolger mehr haben), und setzt dann die spätesten Fertigstellungstermine der jeweils vorgelagerten Vorgänge fest (Rückwärtsplanung). Durch Kombination beider Methoden, ausgehend von einem definierten Start- und einem definierten Endtermin, ergeben sich dadurch für jeden Vorgang neben der Dauer folgende vier weitere wichtige Eigenschaften:

.. image:: figure/projekt/NetzplanKnoten.png


- Frühester Anfangszeitpunkt (FAZ) (aus Vorwärtsplanung)
- Frühester Endzeitpunkt (FEZ) (aus Vorwärtsplanung und jeweiliger Dauer)
- Spätester Endzeitpunkt (SEZ) (aus Rückwärtsplanung)
- Spätester Anfangszeitpunkt (SAZ) (aus Rückwärtsplanung und jeweiliger Dauer)
    

.. image:: figure/projekt/netzplan_beispiel_ohne_loesung.jpg


    

.. only:: latex   

	.. raw:: latex
	
		\newpage

**Regeln:**


#. Die Abhängigkeit zwischen zwei Vorgängen wird durch einen Pfeil dargestellt.    
   Die Pfeilrichtung gibt dabei die Reihenfolge der Vorgänge an.
#. Ein Vorgang kann einen oder mehrere Vorgänger haben.
   Ein Vorgang kann einen oder mehrere Nachfolger haben.
#. Ein Netzplan darf keine Schleifen enthalten.
#. Vom Projektanfang bis zum Projektende muß mindestens ein ununterbrochener 
   Ablauf vorhanden sein.
#. Der Vorgang am Projektanfang beginnt mit einem FAZ von Null.
#. FEZ = FAZ + Dauer
#. Ein Vorgang kann erst beginnen, nachdem sein Vorgänger abgeschlossen ist. Das 
   bedeutet, daß der FEZ eines Vorgangs gleichzeitig der FAZ aller Nachfolger ist.
#. Besitzt ein Vorgang mehrere Vorgänger, so entspricht der FAZ des Vorgangs dem 
   spätesten FEZ aller Vorgänger.
#. Der FEZ des Vorgangs am Projektende ist gleichzeitig der SEZ des Projekts.
#. SAZ = SEZ - Dauer
#. Der SAZ eines Vorgangs ist gleichzeitig der SEZ aller Vorgänger des Vorgangs.
#. Besitzen mehrere Vorgänge einen gemeinsamen Vorgänger, so entspricht der SEZ 
   des gemeinsamen Vorgängers dem frühesten SAZ aller Nachfolger.


.. only:: latex   

	.. raw:: latex
	
		\newpage
	
	
	
   
- Netzplantechnik, Ablauf- und Terminplan
- Ressourcen- und Kostenplan
- Planoptimierung



Projektplanung 3
-----------------

- Projektplanung mit MS-Project
- Ressourcen- und Kostenplanung mit MS-Project


A