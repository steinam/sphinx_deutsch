0. Name (Name)
1. Ausrichtung (SideAlignment)
2. Lichtschwertfarbe (Lightsaber Color)
3. Gr��e (Height)				- Klein = besser im ausweichen; gro� = besser im blocken


4. Auswirkungen der ausgew�hlten Eigenschaften auf die Attribute:
 - Height: Tall = +Strength, Medium = +Charisma, Small = +Initiative
 - SideAlignment: Sith = +Force, Jedi = +HP
 - Specification: Consular/Sorcerer = +Force & +Initiative, Warrior = +Strength & +HP, Josh = +HP & +Charisma