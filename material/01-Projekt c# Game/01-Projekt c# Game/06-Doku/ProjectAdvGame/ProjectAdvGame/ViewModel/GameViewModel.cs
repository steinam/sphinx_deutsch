﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace ProjectAdvGame
{
    public class GameViewModel : ViewModel
    {
        Hero _myHero;
        ButtonModel _btn1;
        ButtonModel _btn2;
        ButtonModel _btn3;
        InOutPutModel _outPut;
        InOutPutModel _inPut;
        ChoiceHandler _choiceHandler;

        // Bei ButtonClick wird die Property von ChoiceCollection abgefragt - welche Antwort erwartet wird.
        // Dann wird beim Command von ButtonClick die Antwort der jeweiligen Property gesetzt.
        // usw.

        public GameViewModel()
        {
            _myHero = new Hero { HP = 100, Strength = 0, Attack = 0, Charisma = 0, Defense = 0, Force = 0, Initiative = 0, HPPotions = 5 };
            _btn1 = new ButtonModel { BtnText = "new game" };
            _btn2 = new ButtonModel { BtnText = "", ButtonVisibility = Visibility.Collapsed };
            _btn3 = new ButtonModel { BtnText = "", ButtonVisibility = Visibility.Collapsed };
            _outPut = new InOutPutModel();
            _inPut = new InOutPutModel();
            _choiceHandler = new ChoiceHandler();

            StoryBeginning();
        }

        #region Model Constructors
        public Hero myHero
        {
            get
            {
                return _myHero;
            }
            set
            {
                _myHero = value;
            }
        }

        public ChoiceHandler myChoiceHandler
        {
            get
            {
                return _choiceHandler;
            }
            set
            {
                _choiceHandler = value;
            }
        }

        public ButtonModel myButton1
        {
            get { return _btn1; }
            set { _btn1 = value; }
        }

        public ButtonModel myButton2
        {
            get { return _btn2; }
            set { _btn2 = value; }
        }

        public ButtonModel myButton3
        {
            get { return _btn3; }
            set { _btn3 = value; }
        }

        public InOutPutModel myGameTextOutPut
        {
            get { return _outPut; }
            set { _outPut = value; }
        }

        public InOutPutModel myGameTextInPut
        {
            get { return _inPut; }
            set { _inPut = value; }
        }
        #endregion

        #region myGameTextOutPut.Propertys
        public string GameText
        {
            get { return myGameTextOutPut.InputOutputText; }
            set
            {
                if (myGameTextOutPut.InputOutputText == null || myGameTextOutPut.InputOutputText == "")
                    myGameTextOutPut.InputOutputText += value;
                else
                    myGameTextOutPut.InputOutputText += "\n" + value;
                RaisePropertyChanged("GameText");
            }
        }
        #endregion

        #region myGameTextInput.Propertys
        public string GameInput
        {
            get { return myGameTextInPut.InputOutputText; }
            set
            {
                myGameTextInPut.InputOutputText = value;
                RaisePropertyChanged("GameInput");
            }
        }

        public int ExpectedProperty
        {
            get { return myChoiceHandler.ExpectedProperty; }
            set
            {
                myChoiceHandler.ExpectedProperty = value;
                RaisePropertyChanged("ExpectedProperty");
            }
        }
        #endregion

        #region Button.Propertys
        public string ButtonText1
        {
            get { return myButton1.BtnText; }
            set
            {
                myButton1.BtnText = value;
                RaisePropertyChanged("ButtonText1");
                if (value == null || value == "")
                    Button1Visibility = Visibility.Collapsed;
                else
                    Button1Visibility = Visibility.Visible;
            }
        }

        public Visibility Button1Visibility
        {
            get { return myButton1.ButtonVisibility; }
            set
            {
                myButton1.ButtonVisibility = value;
                RaisePropertyChanged("Button1Visibility");
            }
        }

        public string ButtonText2
        {
            get { return myButton2.BtnText; }
            set
            {
                myButton2.BtnText = value;
                RaisePropertyChanged("ButtonText2");
                if (value == null || value == "")
                    Button2Visibility = Visibility.Collapsed;
                else
                    Button2Visibility = Visibility.Visible;
            }
        }

        public Visibility Button2Visibility
        {
            get { return myButton2.ButtonVisibility; }
            set
            {
                myButton2.ButtonVisibility = value;
                RaisePropertyChanged("Button2Visibility");
            }
        }

        public string ButtonText3
        {
            get { return myButton3.BtnText; }
            set
            {
                myButton3.BtnText = value;
                RaisePropertyChanged("ButtonText3");
                if (value == null || value == "")
                    Button3Visibility = Visibility.Collapsed;
                else
                    Button3Visibility = Visibility.Visible;
            }
        }

        public Visibility Button3Visibility
        {
            get { return myButton3.ButtonVisibility; }
            set
            {
                myButton3.ButtonVisibility = value;
                RaisePropertyChanged("Button3Visibility");
            }
        }
        #endregion

        #region Hero.Propertys

        public string HeroProfilepicturePath
        {
            get { return myHero.ProfilepicturePath; }
            set
            {
                myHero.ProfilepicturePath = value;
                RaisePropertyChanged("ProfilepicturePath");
            }
        }

        public string HeroName
        {
            get { return myHero.Name; }
            set
            {
                myHero.Name = value;
                RaisePropertyChanged("HeroName");
            }
        }

        public int HeroInitiative
        {
            get { return myHero.Initiative; }
            set
            {
                myHero.Initiative = value;
                RaisePropertyChanged("HeroInitiative");
            }
        }

        public int HeroForce
        {
            get { return myHero.Force; }
            set
            {
                myHero.Force = value;
                RaisePropertyChanged("HeroForce");
            }
        }

        public int HeroHP
        {
            get { return myHero.HP; }
            set
            {
                if (value <= HeroMaxHP)
                {
                    myHero.HP = value;
                    if (myHero.HP > HeroMaxHP)
                        myHero.HP = HeroMaxHP;
                    RaisePropertyChanged("HeroHP");
                }
            }
        }

        public int HeroMaxHP
        {
            get { return myHero.MaxHP; }
            set
            {
                myHero.MaxHP = value;
                RaisePropertyChanged("HeroMaxHP");
            }
        }

        public int HeroHPPotions
        {
            get { return myHero.HPPotions; }
            set
            {
                myHero.HPPotions = value;
                RaisePropertyChanged("HeroHPPotions");
            }
        }

        public string HeroSpecification
        {
            get { return myHero.Specification; }
            set
            {
                myHero.Specification = value;
                RaisePropertyChanged("HeroSpecification");
            }
        }

        public int HeroSideAlignment
        {
            get { return myHero.SideAlignment; }
            set
            {
                myHero.SideAlignment = value;
                RaisePropertyChanged("HeroSideAlignment");
            }
        }

        #region lightsbaer of hero
        public Lightsaber HeroLightsaber
        {
            get
            {
                return myHero.Lightsaber;
            }
            set
            {
                myHero.Lightsaber = value;
                RaisePropertyChanged("HeroLightsaber");
                RaisePropertyChanged("HeroLightsaberColor");
                RaisePropertyChanged("HeroLightsaberDescription");
                RaisePropertyChanged("HeroLightsaberName");
            }
        }

        public string HeroLightsaberColor
        {
            get
            {
                if (myHero.Lightsaber != null)
                    return myHero.Lightsaber.Color;
                else
                    return "White";
            }
        }

        public string HeroLightsaberDescription
        {
            get
            {
                if (myHero.Lightsaber != null)
                    return myHero.Lightsaber.Description;
                else
                    return "Unknown";
            }
        }

        public string HeroLightsaberName
        {
            get
            {
                if (myHero.Lightsaber != null)
                    return myHero.Lightsaber.Name;
                else
                    return "Unknown";
            }
        }

        public int HeroLightsaberAttack
        {
            get
            {
                if (myHero.Lightsaber != null)
                    return myHero.Lightsaber.Attack;
                else
                    return 0;
            }
        }
        #endregion

        public string HeroHeight
        {
            get { return myHero.Height; }
            set
            {
                myHero.Height = value;
                RaisePropertyChanged("HeroHeight");
            }
        }

        public int HeroStrength
        {
            get { return myHero.Strength; }
            set
            {
                myHero.Strength = value;
                RaisePropertyChanged("HeroStrength");
            }
        }

        public int HeroCharisma
        {
            get { return myHero.Charisma; }
            set
            {
                myHero.Charisma = value;
                RaisePropertyChanged("HeroCharisma");
            }
        }

        public int HeroDefense
        {
            get { return myHero.Defense; }
            set
            {
                myHero.Defense = value;
                RaisePropertyChanged("HeroDefense");
            }
        }

        public int HeroAttack
        {
            get { return myHero.Attack; }
            set
            {
                myHero.Attack = value;
                RaisePropertyChanged("HeroAttack");
            }
        }

        #endregion

        public

        #region Commands
 void IncreaseHPExecute()
        {
            HeroHP = HeroHP + 1;
        }

        bool CanIncreaseHPExecute()
        {
            return true;
        }


        public ICommand IncreaseHP { get { return new RelayCommand(IncreaseHPExecute, CanIncreaseHPExecute); } }

        void TakeHPPotionExecute()
        {
            HeroHP = HeroHP + 15;
            HeroHPPotions--;
        }

        bool CanTakeHPPotionExecute()
        {
            if (HeroHPPotions > 0 && HeroHP < HeroMaxHP)
                return true;
            else
                return false;
        }

        public ICommand TakeHPPotion { get { return new RelayCommand(TakeHPPotionExecute, CanTakeHPPotionExecute); } }

        void ClickSendBtnexecute()
        {
            SetChoice(GameInput);
            GameInput = "";
        }

        bool CanClickSendBtnexecute()
        {
            if (GameInput != "" && GameInput != null)
                return true;
            else
                return false;
        }

        public ICommand ClikSendBtn { get { return new RelayCommand(ClickSendBtnexecute, CanClickSendBtnexecute); } }

        // Butotn #1
        void ClickButton1Execute()
        {
            SetChoice(ButtonText1);
        }

        bool CanClickButton1Execute()
        {
            if (ButtonText1 != "" && ButtonText1 != null)
                return true;
            else
                return false;
        }

        public ICommand ClickButton1 { get { return new RelayCommand(ClickButton1Execute, CanClickButton1Execute); } }

        // Butotn #2
        void ClickButton2Execute()
        {
            SetChoice(ButtonText2);
        }

        bool CanClickButton2Execute()
        {
            if (ButtonText2 != "" && ButtonText2 != null)
                return true;
            else
                return false;
        }

        public ICommand ClickButton2 { get { return new RelayCommand(ClickButton2Execute, CanClickButton2Execute); } }

        // Button #3
        void ClickButton3Execute()
        {
            SetChoice(ButtonText3);
        }

        bool CanClickButton3Execute()
        {
            if (ButtonText3 != "" && ButtonText3 != null)
                return true;
            else
                return false;
        }

        public ICommand ClickButton3 { get { return new RelayCommand(ClickButton3Execute, CanClickButton3Execute); } }

        #endregion

        #region methods
        private void StoryBeginning()
        {
            GameText = "Welcome back Hero!";
            ExpectedProperty = 0;
            //ShowChoice(new Choice("Whats your name? (If you don't know your name write: \"random\")", "My Name", "Name"));
        }

        private void ShowChoice(Choice choice)
        {
            GameText = choice.Questiontext;
            ButtonText1 = choice.Answer1;
            ButtonText2 = choice.Answer2;
            ButtonText3 = choice.Answer3;
        }

        private void SetChoice(string choice)
        {
            switch (ExpectedProperty)
            {
                case 0:
                    //HeroName = choice;
                    //GameText = "Welcome to your journey " + HeroName;
                    GameText = "Please say me what side you favor most?";
                    ButtonText1 = Properties.Resources.SideJedi;
                    ButtonText2 = Properties.Resources.SideSith;
                    ButtonText3 = "";
                    ExpectedProperty += 1;
                    break;
                case 1:
                    //HeroHeight = choice;
                    if (choice == ButtonText1)
                        HeroSideAlignment = 50;
                    else if (choice == ButtonText2)
                        HeroSideAlignment = -50;

                    if (HeroSideAlignment > 0)
                    {
                        ButtonText1 = Properties.Resources.SpecJedi1; // = Sorcerer
                        ButtonText2 = Properties.Resources.SpecJedi2; // = Warrior
                        ButtonText3 = Properties.Resources.SpecJedi3; // = Josh
                    }
                    if (HeroSideAlignment < 0)
                    {
                        ButtonText1 = Properties.Resources.SpecSith1; // = Consular
                        ButtonText2 = Properties.Resources.SpecSith2; // = Warrior
                        ButtonText3 = Properties.Resources.SpecSith3; // = Josh
                    }
                    ExpectedProperty += 1;
                    GameText = "Now you can choose your specification! Consider your choice wisely!";
                    break;
                case 2:
                    HeroSpecification = choice;

                    if (HeroSideAlignment > 0)
                    {
                        ButtonText1 = "Blue";
                        ButtonText2 = "Green";
                        ButtonText3 = "Yellow";
                    }
                    else if (HeroSideAlignment < 0)
                    {
                        ButtonText1 = "Red";
                        ButtonText2 = "Orange";
                        ButtonText3 = "Black";
                    }
                    ExpectedProperty += 1;
                    GameText = "Now you can choose your lightsaber!";
                    break;
                case 3:
                    Lightsaber tempLightsaber = new Lightsaber();
                    tempLightsaber.Attack = 10;
                    tempLightsaber.Color = choice;
                    tempLightsaber.Defense = 5;
                    tempLightsaber.Description = "Ultimate beginner lightsaber";
                    tempLightsaber.Name = "First Lightsaber";
                    HeroLightsaber = tempLightsaber;

                    ButtonText1 = Properties.Resources.HeightSmall;
                    ButtonText2 = Properties.Resources.HeightMid;
                    ButtonText3 = Properties.Resources.HeightTall;
                    ExpectedProperty += 1;
                    GameText = "Choose your tallness!";
                    break;
                case 4:
                    HeroHeight = choice;

                    if (!HeroSpecification.Contains("Josh"))
                    {
                        //ButtonText1 = "Custom";
                        ButtonText1 = "";
                        ButtonText2 = "";
                        ButtonText3 = "Random";
                        ExpectedProperty += 1;
                        GameText = "It's time to choose a name, even if you have no choice!";
                    }
                    else
                    {
                        HeroName = "Josh";
                        ExpectedProperty += 1;
                    }
                    break;
                case 5:
                    if (HeroName == "" || HeroName == null)
                        HeroName = choice;
                    // Attribute zuordnen
                    SetHeroAttributes();

                    // Bild hinzufügen
                    HeroProfilepicturePath = SetProfilepicture(HeroName, HeroSideAlignment);

                    ButtonText1 = "Start journey";
                    ExpectedProperty += 1;
                    GameText = "Your Hero is now created!";
                    break;
            }
        }

        private void SetHeroAttributes()
        {
            // main dice for standard attributes
            Dice mainDice = new Dice(30, 40);
            // bonus dice for bonus of the attributes
            Dice bonusDice = new Dice(5, 10);

            HeroStrength = mainDice.RollDice();
            HeroCharisma = mainDice.RollDice();
            HeroInitiative = mainDice.RollDice();
            HeroForce = mainDice.RollDice();
            //force = Dice.RollDice();

            // BONUS

            // HEIGHT BONUS
            // if Hero Height = small => initiative
            if (HeroHeight == Properties.Resources.HeightSmall)
            {
                HeroInitiative += bonusDice.RollDice();
            }
            // if Hero Height = mid => charisma
            else if (HeroHeight == Properties.Resources.HeightMid)
            {
                HeroCharisma += bonusDice.RollDice();
            }
            // if Hero Height = tall => strength
            else if (HeroHeight == Properties.Resources.HeightTall)
            {
                HeroStrength += bonusDice.RollDice();
            }

            // SIDE BONUS (Jedi/Sith)
            // if jedi => +HP
            if (HeroSideAlignment > 0)
            {
                HeroMaxHP += bonusDice.RollDice();
            }
            // if sith => +Force
            else if (HeroSideAlignment < 0)
            {
                HeroStrength += bonusDice.RollDice();
            }

            // SPECIFICATION BONUS (Consular/Sorcerer, Warrior, Josh)
            // Consular/Sorcerer => +Force & +Initiative
            if (HeroSpecification == Properties.Resources.SpecJedi1 || HeroSpecification == Properties.Resources.SpecSith1)
            {
                HeroForce += bonusDice.RollDice();
                HeroInitiative += bonusDice.RollDice();
            }
            // Warrior => +Strength & +MaxHP
            else if (HeroSpecification == Properties.Resources.SpecSith2 || HeroSpecification == Properties.Resources.SpecJedi2)
            {
                HeroStrength += bonusDice.RollDice();
                HeroMaxHP += bonusDice.RollDice();
            }
            // Josh => +HPPotions & +Charisma
            else if (HeroSpecification == Properties.Resources.SpecJedi3 || HeroSpecification == Properties.Resources.SpecSith3)
            {
                HeroHPPotions += bonusDice.RollDice();
                HeroCharisma += bonusDice.RollDice();
            }
        }

        private string SetProfilepicture(string name, int sideAlignment)
        {
            string path = "";

            if (name.ToLower() == "josh")
            {
                if (sideAlignment > 0)
                    path = @"Pics\Profiles\josh_jedi.png";
                else
                    path = @"Pics\Profiles\josh_sith.png";
            }

            return path;
        }
        #endregion
    }
}
