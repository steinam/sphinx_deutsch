﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectAdvGame
{
    public class Equipment
    {
        private string _Name = "Unknown";
        /// <summary>
        /// Name des Equipments
        /// </summary>
        public string Name
        {
            get
            {
                return _Name;
            }
            set
            {
                _Name = value;
            }
        }

        private string _Description = "Unknown";
        /// <summary>
        /// Beschreibung des Equipments
        /// </summary>
        public string Description
        {
            get
            {
                return _Description;
            }
            set
            {
                _Description = value;
            }
        }

        private Int16 _Defense = 0;
        /// <summary>
        /// Verteidigungswert des Equipments
        /// </summary>
        public Int16 Defense
        {
            get
            {
                return _Defense;
            }
            set
            {
                _Defense = value;
            }
        }

        private Int16 _Attack = 0;
        /// <summary>
        /// Angriffswert des Equipments
        /// </summary>
        public Int16 Attack
        {
            get
            {
                return _Attack;
            }
            set
            {
                _Attack = value;
            }
        }
    }
}
