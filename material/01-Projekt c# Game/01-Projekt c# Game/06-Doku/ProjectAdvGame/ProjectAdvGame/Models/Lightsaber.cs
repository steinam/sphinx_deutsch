﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectAdvGame
{
    public class Lightsaber : Equipment
    {
        private string _Color;
        /// <summary>
        /// Farbe des Lichtschwertes
        /// </summary>
        public string Color
        {
            get
            {
                return _Color;
            }
            set
            {
                _Color = value;
            }
        } 
    }
}
