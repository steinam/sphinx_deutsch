﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace ProjectAdvGame
{
    public class ButtonModel
    {
        private string _btnText;
        /// <summary>
        /// Text / Content des Buttons
        /// </summary>
        public string BtnText
        {
            get { return _btnText; }
            set { _btnText = value; }
        }

        private Visibility _buttonVisibility;
        /// <summary>
        /// Sichtbarkeit des Buttons
        /// </summary>
        public Visibility ButtonVisibility
        {
            get { return _buttonVisibility; }
            set
            {
                _buttonVisibility = value;
            }
        }
    }
}
