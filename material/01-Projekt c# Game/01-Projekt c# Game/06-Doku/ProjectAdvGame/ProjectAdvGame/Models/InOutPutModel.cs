﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectAdvGame
{
    public class InOutPutModel
    {
        private string _inputoutputText;
        /// <summary>
        /// Text des Ausgabefeldes
        /// </summary>
        public string InputOutputText
        {
            get
            {
                return _inputoutputText;
            }
            set
            {
                _inputoutputText = value;
            }
        }
    }
}
