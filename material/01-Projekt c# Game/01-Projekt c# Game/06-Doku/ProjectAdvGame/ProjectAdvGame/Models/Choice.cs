﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectAdvGame
{

    //Beinhaltet einen Fragentext und verschiedene Antwortmöglichkeiten

    public class Choice
    {
        /// <summary>
        /// Initialisiert eine neue Instanz der Choice Klasse
        /// </summary>
        /// <param name="question">Frage- / Antworttext</param>
        /// <param name="answer1">Antworttext 1</param>
        /// <param name="answer2">Antworttext 2</param>
        /// <param name="answer3">Antworttext 3</param>
        /// <param name="expectedProperty">Eigenschaft die geändert wird</param>
        public Choice(string question, string answer1, string answer2, string answer3, string expectedProperty)
        {
            Questiontext = question;
            Answer1 = answer1;
            Answer2 = answer2;
            Answer3 = answer3;
            UseForProperty = expectedProperty;
        }

        /// <summary>
        /// Initialisiert eine neue Instanz der Choice Klasse
        /// </summary>
        /// <param name="question">Frage- / Antworttext</param>
        /// <param name="answer1">Antworttext 1</param>
        /// <param name="answer2">Antworttext 2</param>
        /// <param name="expectedProperty">Eigenschaft die geändert wird</param>
        public Choice(string question, string answer1, string answer2, string expectedProperty)
        {
            Questiontext = question;
            Answer1 = answer1;
            Answer2 = answer2;
            UseForProperty = expectedProperty;
        }

        /// <summary>
        /// Initialisiert eine neue Instanz der Choice Klasse
        /// </summary>
        /// <param name="question">Frage- / Antworttext</param>
        /// <param name="answer1">Antworttext 1</param>
        /// <param name="expectedProperty">Eigenschaft die geändert wird</param>
        public Choice(string question, string answer1, string expectedProperty)
        {
            Questiontext = question;
            Answer1 = answer1;
            UseForProperty = expectedProperty;
        }

        
        private string _questiontext;
        /// <summary>
        /// Text der Frage für die Choice
        /// </summary>
        public string Questiontext
        {
            get { return _questiontext; }
            set { _questiontext = value; }
        }

        private string _answer1;
        /// <summary>
        /// Inhalt der dritten Antwortmöglichkeit
        /// </summary>
        public string Answer1
        {
            get { return _answer1; }
            set { _answer1 = value; }
        }

        private string _answer2;
        /// <summary>
        /// Inhalt der zweiten Antwortmöglichkeit
        /// </summary>
        public string Answer2
        {
            get { return _answer2; }
            set { _answer2 = value; }
        }

        private string _answer3;
        /// <summary>
        /// Inhalt der dritten Antwortmöglichkeit
        /// </summary>
        public string Answer3
        {
            get { return _answer3; }
            set { _answer3 = value; }
        }

        private string _useForProperty;
        /// <summary>
        /// Gibt die Eigenschaft an, die geändert wird bei dieser Choice
        /// </summary>
        public string UseForProperty
        {
            get { return _useForProperty; }
            set
            {
                _useForProperty = value;
            }
        }
    }
}