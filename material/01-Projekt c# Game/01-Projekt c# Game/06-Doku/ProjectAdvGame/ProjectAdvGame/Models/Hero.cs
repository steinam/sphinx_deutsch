﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectAdvGame
{
    public class Hero
    {
        /// <remarks>Sets the size of the hero. Can't change during a running game.</remarks>
        private string _Height = "Unknown";
        /// <summary>
        /// Angabe des Größenwertes (Small, Medium, Tall)
        /// </summary>
        public string Height
        {
            get
            {
                return _Height;
            }
            set
            {
                _Height = value;
            }
        }

        private Lightsaber _lightsaber;
        /// <summary>
        /// Instanz der Lightsaber Klasse
        /// </summary>
        public Lightsaber Lightsaber
        {
            get
            {
                return _lightsaber;
            }
            set
            {
                _lightsaber = value;
            }
        }

        public List<Object> Inventory { get; set; }

        /// <remarks>Sets the Health-Status of the hero.</remarks>
        /// <value>100</value>
        /// 
        private int _hp = 100;
        /// <summary>
        /// Gibt die Lebenspunkte des Helden an
        /// </summary>
        public int HP
        {
            get
            {
                return _hp;
            }
            set
            {
                _hp = value;
                if (value > MaxHP)
                    _hp = MaxHP;
            }
        }

        private int _maxHP = 100;
        /// <summary>
        /// Gibt die maximale Lebenspunktezahl des Helden an
        /// </summary>
        public int MaxHP
        {
            get
            {
                return _maxHP;
            }
            set
            {
                _maxHP = value;
            }
        }

        private string _ProfilepicturePath = "";
        /// <summary>
        /// Bildpfad des Profils
        /// </summary>
        public string ProfilepicturePath
        {
            get { return _ProfilepicturePath; }
            set
            {
                _ProfilepicturePath = value;
            }
        }

        private string _Name = "Unknown";
        /// <summary>
        /// Name des Helden.
        /// </summary>
        public string Name
        {
            get
            {
                return _Name;
            }
            set
            {
                if (value.ToLower() == "random")
                {
                    _Name = RandomName();
                }
                else
                {
                    _Name = SetName(value);
                }
            }
        }

        /// <remarks>Hero can earn dark side or light side points -> Change orientation / alignment during the game!</remarks>
        /// <value>0</value>
        private int _SideAlignment = 0;
        /// <summary>
        /// Ausrichtung des Helden (Hell, Dunkel)
        /// </summary>
        public int SideAlignment
        {
            get
            {
                return _SideAlignment;
            }
            set
            {
                _SideAlignment = value;
            }
        }

        //Klasse (z.B Kopfgeldjäger)
        private string _Specification = "Unknown";
        /// <summary>
        /// Spezifikation (z.B. Kopfgeldjäger, Jedi-Ritter, etc.)
        /// </summary>
        public string Specification
        {
            get
            {
                return _Specification;
            }
            set
            {
                _Specification = value;
            }
        }

        private int _Strength = 0;
        /// <summary>
        /// Stärke des Helden
        /// </summary>
        public int Strength
        {
            get
            {
                return _Strength;
            }
            set
            {
                _Strength = value;
            }
        }

        private int _Force = 0;
        /// <summary>
        /// Machtwert des Helden
        /// </summary>
        public int Force
        {
            get
            {
                return _Force;
            }
            set
            {
                _Force = value;
            }
        }

        // Increases the chance to have the first strike.
        private int _Initiative = 0;
        /// <summary>
        /// Initiativewert des Helden (Hat Auswirkung auf die Start Reihenfolge beim Kampf)
        /// </summary>
        public int Initiative
        {
            get
            {
                return _Initiative;
            }
            set
            {
                _Initiative = value;
            }
        }

        private int _Charisma = 0;
        /// <summary>
        /// Charismawert des Helden
        /// </summary>
        public int Charisma
        {
            get
            {
                return _Charisma;
            }
            set
            {
                _Charisma = value;
            }
        }

        private int _Defense = 0;
        /// <summary>
        /// Verteidigungswert des Helden
        /// </summary>
        public int Defense
        {
            get
            {
                return _Defense;
            }
            set
            {
                _Defense = value;
            }
        }

        private int _Attack = 0;
        /// <summary>
        /// Angriffswert des Helden
        /// </summary>
        public int Attack
        {
            get
            {
                return _Attack;
            }
            set
            {
                _Attack = value;
            }
        }

        private int _hPPotions = 0;
        /// <summary>
        /// Anzahl der Lebenstränke
        /// </summary>
        public int HPPotions
        {
            get
            {
                return _hPPotions;
            }
            set
            {
                _hPPotions = value;
            }
        }

        /// <summary>
        /// Setzt den Namen, falls der Name unter 3 Buchstaben liegt wird er befüllt.
        /// </summary>
        /// <param name="value">Name</param>
        /// <returns>Voller Name</returns>
        private string SetName(string value)
        {
            string tempName = value;
            while (value.Length < 3)
            {
                tempName = tempName + value;
            }
            return tempName;
        }

        private Random rndNumber = new Random();
        /// <summary>
        /// Gibt einen zufälligen Namen zurück
        /// </summary>
        /// <returns></returns>
        private string RandomName()
        {
            string rndName = "";
            List<string> nameList = new List<string>() { "Ben Kehnobi", "Han Slolo", "Luke SykeWalker", "Darth Hater", "Darth Zidius", "Tschubaka" , "C3P0", "Prinzessin Leyah"};
            
            rndName = nameList[rndNumber.Next(0, nameList.Count)];

            return rndName;
        }
    }
}
