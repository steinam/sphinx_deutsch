﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectAdvGame
{
    public class Dice
    {
        private Random rdn;
        private int _max;
        private int _min;

        public Dice(int min, int max)
        {
            rdn = new Random();
            _min = min;
            _max = max;
        }

        public int RollDice()
        {
            int result = -1;

            result = Roll();

            return result;
        }

        public int RollDice(int diceCount)
        {
            int result = -1;

            for (int i = 0; i < diceCount; i++)
            {
                result += Roll();
            }

            return result;
        }

        private int Roll()
        {
            return rdn.Next(_min, _max);
        }
    }
}
