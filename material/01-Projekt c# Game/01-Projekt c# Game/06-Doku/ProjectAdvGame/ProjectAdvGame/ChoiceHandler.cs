﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectAdvGame
{
    public class ChoiceHandler
    {
        private int _expectedProperty;
        public int ExpectedProperty
        {
            get
            {
                return _expectedProperty;
            }
            set
            {
                _expectedProperty = value;
            }
        }
    }
}
