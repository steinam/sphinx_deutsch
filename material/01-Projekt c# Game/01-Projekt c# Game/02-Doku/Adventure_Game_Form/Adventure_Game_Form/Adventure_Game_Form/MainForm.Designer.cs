﻿namespace Adventure_Game_Form
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.tabControl_Invisible1 = new TabControl_Invisible();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.button5 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.create_Character1 = new Adventure_Game_Form.Class_Form.Create_Character();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.tabControl_Invisible2 = new TabControl_Invisible();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.map1 = new Adventure_Game_Form.Class_Form.map();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.cmdItemshop = new System.Windows.Forms.Button();
            this.cmdManatrank = new System.Windows.Forms.Button();
            this.cmdLebenstrank = new System.Windows.Forms.Button();
            this.cmdAngriff = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.labelGold = new System.Windows.Forms.Label();
            this.button7 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.labelSonstiges = new System.Windows.Forms.Label();
            this.labelWaffe = new System.Windows.Forms.Label();
            this.labelRüstung = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.labelManaDisplay = new System.Windows.Forms.Label();
            this.labelStaminaDisplay = new System.Windows.Forms.Label();
            this.labelLebenDisplay = new System.Windows.Forms.Label();
            this.progressBarMana = new System.Windows.Forms.ProgressBar();
            this.progressBarStamina = new System.Windows.Forms.ProgressBar();
            this.progressBarLife = new System.Windows.Forms.ProgressBar();
            this.label21 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.labelLevel = new System.Windows.Forms.Label();
            this.labelKlasse = new System.Windows.Forms.Label();
            this.labeHeroName = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.lblGold = new System.Windows.Forms.Label();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.tabPage6 = new System.Windows.Forms.TabPage();
            this.tabControl_Invisible1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.tabControl_Invisible2.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.tabPage5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "profession_human_warrior.png");
            this.imageList1.Images.SetKeyName(1, "profession_human_mage.png");
            this.imageList1.Images.SetKeyName(2, "profession_elf_ranger.png");
            this.imageList1.Images.SetKeyName(3, "profession_dwarf_mercenary.png");
            // 
            // tabControl_Invisible1
            // 
            this.tabControl_Invisible1.Controls.Add(this.tabPage2);
            this.tabControl_Invisible1.Controls.Add(this.tabPage1);
            this.tabControl_Invisible1.Controls.Add(this.tabPage3);
            this.tabControl_Invisible1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl_Invisible1.Location = new System.Drawing.Point(0, 0);
            this.tabControl_Invisible1.Name = "tabControl_Invisible1";
            this.tabControl_Invisible1.SelectedIndex = 0;
            this.tabControl_Invisible1.Size = new System.Drawing.Size(835, 537);
            this.tabControl_Invisible1.TabIndex = 0;
            // 
            // tabPage2
            // 
            this.tabPage2.BackColor = System.Drawing.Color.Tan;
            this.tabPage2.Controls.Add(this.button5);
            this.tabPage2.Controls.Add(this.button4);
            this.tabPage2.Controls.Add(this.button3);
            this.tabPage2.Controls.Add(this.label4);
            this.tabPage2.Controls.Add(this.label3);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(827, 511);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "tabPage2";
            // 
            // button5
            // 
            this.button5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.button5.Font = new System.Drawing.Font("Century Schoolbook", 18F, System.Drawing.FontStyle.Bold);
            this.button5.Location = new System.Drawing.Point(263, 133);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(281, 50);
            this.button5.TabIndex = 11;
            this.button5.Text = "Neues Spiel ";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // button4
            // 
            this.button4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.button4.Font = new System.Drawing.Font("Century Schoolbook", 18F, System.Drawing.FontStyle.Bold);
            this.button4.Location = new System.Drawing.Point(263, 242);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(281, 50);
            this.button4.TabIndex = 10;
            this.button4.Text = "Spiel laden";
            this.button4.UseVisualStyleBackColor = true;
            // 
            // button3
            // 
            this.button3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.button3.Font = new System.Drawing.Font("Century Schoolbook", 18F, System.Drawing.FontStyle.Bold);
            this.button3.Location = new System.Drawing.Point(263, 352);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(281, 50);
            this.button3.TabIndex = 9;
            this.button3.Text = "Beenden";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Century Schoolbook", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(322, 47);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(140, 25);
            this.label4.TabIndex = 4;
            this.label4.Text = "Hauptmenü";
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Century Schoolbook", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(257, 13);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(270, 34);
            this.label3.TabIndex = 3;
            this.label3.Text = "Adventure Game";
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.Color.Tan;
            this.tabPage1.Controls.Add(this.create_Character1);
            this.tabPage1.Controls.Add(this.button2);
            this.tabPage1.Controls.Add(this.button1);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(827, 511);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "tabPage1";
            // 
            // create_Character1
            // 
            this.create_Character1.Location = new System.Drawing.Point(3, 3);
            this.create_Character1.Name = "create_Character1";
            this.create_Character1.Size = new System.Drawing.Size(816, 452);
            this.create_Character1.TabIndex = 10;
            // 
            // button2
            // 
            this.button2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.button2.Font = new System.Drawing.Font("Century Schoolbook", 18F, System.Drawing.FontStyle.Bold);
            this.button2.Location = new System.Drawing.Point(3, 458);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(212, 50);
            this.button2.TabIndex = 9;
            this.button2.Text = "Zurücksetzen";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.button1.Font = new System.Drawing.Font("Century Schoolbook", 18F, System.Drawing.FontStyle.Bold);
            this.button1.Location = new System.Drawing.Point(667, 458);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(144, 50);
            this.button1.TabIndex = 8;
            this.button1.Text = "Starten";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // tabPage3
            // 
            this.tabPage3.BackColor = System.Drawing.Color.Tan;
            this.tabPage3.Controls.Add(this.tabControl_Invisible2);
            this.tabPage3.Controls.Add(this.groupBox3);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(827, 511);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "tabPage3";
            // 
            // tabControl_Invisible2
            // 
            this.tabControl_Invisible2.Controls.Add(this.tabPage4);
            this.tabControl_Invisible2.Controls.Add(this.tabPage5);
            this.tabControl_Invisible2.Controls.Add(this.tabPage6);
            this.tabControl_Invisible2.Location = new System.Drawing.Point(8, 3);
            this.tabControl_Invisible2.Name = "tabControl_Invisible2";
            this.tabControl_Invisible2.SelectedIndex = 0;
            this.tabControl_Invisible2.Size = new System.Drawing.Size(810, 363);
            this.tabControl_Invisible2.TabIndex = 2;
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.map1);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(802, 337);
            this.tabPage4.TabIndex = 0;
            this.tabPage4.Text = "tabPage4";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // map1
            // 
            this.map1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.map1.Location = new System.Drawing.Point(3, 3);
            this.map1.Name = "map1";
            this.map1.Size = new System.Drawing.Size(796, 331);
            this.map1.TabIndex = 8;
            this.map1.CharacterMoved += new System.EventHandler(this.map1_CharacterMoved);
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.progressBar1);
            this.tabPage5.Controls.Add(this.pictureBox2);
            this.tabPage5.Controls.Add(this.cmdItemshop);
            this.tabPage5.Controls.Add(this.cmdManatrank);
            this.tabPage5.Controls.Add(this.cmdLebenstrank);
            this.tabPage5.Controls.Add(this.cmdAngriff);
            this.tabPage5.Controls.Add(this.pictureBox1);
            this.tabPage5.Location = new System.Drawing.Point(4, 22);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage5.Size = new System.Drawing.Size(802, 337);
            this.tabPage5.TabIndex = 1;
            this.tabPage5.Text = "tabPage5";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // cmdItemshop
            // 
            this.cmdItemshop.Location = new System.Drawing.Point(249, 308);
            this.cmdItemshop.Name = "cmdItemshop";
            this.cmdItemshop.Size = new System.Drawing.Size(75, 23);
            this.cmdItemshop.TabIndex = 4;
            this.cmdItemshop.Text = "Itemshop";
            this.cmdItemshop.UseVisualStyleBackColor = true;
            this.cmdItemshop.Click += new System.EventHandler(this.cmdItemshop_Click);
            // 
            // cmdManatrank
            // 
            this.cmdManatrank.Location = new System.Drawing.Point(168, 308);
            this.cmdManatrank.Name = "cmdManatrank";
            this.cmdManatrank.Size = new System.Drawing.Size(75, 23);
            this.cmdManatrank.TabIndex = 3;
            this.cmdManatrank.Text = "Mana-Trank";
            this.cmdManatrank.UseVisualStyleBackColor = true;
            this.cmdManatrank.Click += new System.EventHandler(this.cmdManatrank_Click);
            // 
            // cmdLebenstrank
            // 
            this.cmdLebenstrank.Location = new System.Drawing.Point(87, 308);
            this.cmdLebenstrank.Name = "cmdLebenstrank";
            this.cmdLebenstrank.Size = new System.Drawing.Size(75, 23);
            this.cmdLebenstrank.TabIndex = 2;
            this.cmdLebenstrank.Text = "HP-Trank";
            this.cmdLebenstrank.UseVisualStyleBackColor = true;
            this.cmdLebenstrank.Click += new System.EventHandler(this.cmdLebenstrank_Click);
            // 
            // cmdAngriff
            // 
            this.cmdAngriff.Location = new System.Drawing.Point(6, 308);
            this.cmdAngriff.Name = "cmdAngriff";
            this.cmdAngriff.Size = new System.Drawing.Size(75, 23);
            this.cmdAngriff.TabIndex = 1;
            this.cmdAngriff.Text = "Angriff";
            this.cmdAngriff.UseVisualStyleBackColor = true;
            this.cmdAngriff.Click += new System.EventHandler(this.cmdAngriff_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(6, 252);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(100, 50);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // groupBox3
            // 
            this.groupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.groupBox3.Controls.Add(this.lblGold);
            this.groupBox3.Controls.Add(this.labelGold);
            this.groupBox3.Controls.Add(this.button7);
            this.groupBox3.Controls.Add(this.button6);
            this.groupBox3.Controls.Add(this.labelSonstiges);
            this.groupBox3.Controls.Add(this.labelWaffe);
            this.groupBox3.Controls.Add(this.labelRüstung);
            this.groupBox3.Controls.Add(this.label24);
            this.groupBox3.Controls.Add(this.label23);
            this.groupBox3.Controls.Add(this.label22);
            this.groupBox3.Controls.Add(this.labelManaDisplay);
            this.groupBox3.Controls.Add(this.labelStaminaDisplay);
            this.groupBox3.Controls.Add(this.labelLebenDisplay);
            this.groupBox3.Controls.Add(this.progressBarMana);
            this.groupBox3.Controls.Add(this.progressBarStamina);
            this.groupBox3.Controls.Add(this.progressBarLife);
            this.groupBox3.Controls.Add(this.label21);
            this.groupBox3.Controls.Add(this.label20);
            this.groupBox3.Controls.Add(this.label19);
            this.groupBox3.Controls.Add(this.labelLevel);
            this.groupBox3.Controls.Add(this.labelKlasse);
            this.groupBox3.Controls.Add(this.labeHeroName);
            this.groupBox3.Controls.Add(this.label18);
            this.groupBox3.Controls.Add(this.label17);
            this.groupBox3.Controls.Add(this.label16);
            this.groupBox3.Font = new System.Drawing.Font("Century Schoolbook", 14.25F, System.Drawing.FontStyle.Bold);
            this.groupBox3.Location = new System.Drawing.Point(8, 372);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(810, 131);
            this.groupBox3.TabIndex = 7;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Held";
            // 
            // labelGold
            // 
            this.labelGold.AutoSize = true;
            this.labelGold.Font = new System.Drawing.Font("Century Schoolbook", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelGold.Location = new System.Drawing.Point(6, 112);
            this.labelGold.Name = "labelGold";
            this.labelGold.Size = new System.Drawing.Size(50, 19);
            this.labelGold.TabIndex = 24;
            this.labelGold.Text = "Gold:";
            // 
            // button7
            // 
            this.button7.Location = new System.Drawing.Point(629, 97);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(84, 28);
            this.button7.TabIndex = 23;
            this.button7.Text = "frei";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(719, 97);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(84, 28);
            this.button6.TabIndex = 22;
            this.button6.Text = "Kampf";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // labelSonstiges
            // 
            this.labelSonstiges.AutoSize = true;
            this.labelSonstiges.Font = new System.Drawing.Font("Century Schoolbook", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelSonstiges.Location = new System.Drawing.Point(554, 85);
            this.labelSonstiges.Name = "labelSonstiges";
            this.labelSonstiges.Size = new System.Drawing.Size(135, 19);
            this.labelSonstiges.TabIndex = 21;
            this.labelSonstiges.Text = "Max Musterman";
            // 
            // labelWaffe
            // 
            this.labelWaffe.AutoSize = true;
            this.labelWaffe.Font = new System.Drawing.Font("Century Schoolbook", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelWaffe.Location = new System.Drawing.Point(554, 55);
            this.labelWaffe.Name = "labelWaffe";
            this.labelWaffe.Size = new System.Drawing.Size(135, 19);
            this.labelWaffe.TabIndex = 20;
            this.labelWaffe.Text = "Max Musterman";
            // 
            // labelRüstung
            // 
            this.labelRüstung.AutoSize = true;
            this.labelRüstung.Font = new System.Drawing.Font("Century Schoolbook", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelRüstung.Location = new System.Drawing.Point(554, 26);
            this.labelRüstung.Name = "labelRüstung";
            this.labelRüstung.Size = new System.Drawing.Size(135, 19);
            this.labelRüstung.TabIndex = 19;
            this.labelRüstung.Text = "Max Musterman";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Century Schoolbook", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(470, 84);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(87, 19);
            this.label24.TabIndex = 18;
            this.label24.Text = "Sonstiges:";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Century Schoolbook", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(470, 55);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(58, 19);
            this.label23.TabIndex = 17;
            this.label23.Text = "Waffe:";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Century Schoolbook", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(470, 26);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(78, 19);
            this.label22.TabIndex = 16;
            this.label22.Text = "Rüstung:";
            // 
            // labelManaDisplay
            // 
            this.labelManaDisplay.AutoSize = true;
            this.labelManaDisplay.Font = new System.Drawing.Font("Century Schoolbook", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelManaDisplay.Location = new System.Drawing.Point(412, 84);
            this.labelManaDisplay.Name = "labelManaDisplay";
            this.labelManaDisplay.Size = new System.Drawing.Size(52, 15);
            this.labelManaDisplay.TabIndex = 15;
            this.labelManaDisplay.Text = "100/100";
            // 
            // labelStaminaDisplay
            // 
            this.labelStaminaDisplay.AutoSize = true;
            this.labelStaminaDisplay.Font = new System.Drawing.Font("Century Schoolbook", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelStaminaDisplay.Location = new System.Drawing.Point(412, 55);
            this.labelStaminaDisplay.Name = "labelStaminaDisplay";
            this.labelStaminaDisplay.Size = new System.Drawing.Size(52, 15);
            this.labelStaminaDisplay.TabIndex = 14;
            this.labelStaminaDisplay.Text = "100/100";
            // 
            // labelLebenDisplay
            // 
            this.labelLebenDisplay.AutoSize = true;
            this.labelLebenDisplay.Font = new System.Drawing.Font("Century Schoolbook", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelLebenDisplay.Location = new System.Drawing.Point(412, 26);
            this.labelLebenDisplay.Name = "labelLebenDisplay";
            this.labelLebenDisplay.Size = new System.Drawing.Size(52, 15);
            this.labelLebenDisplay.TabIndex = 13;
            this.labelLebenDisplay.Text = "100/100";
            // 
            // progressBarMana
            // 
            this.progressBarMana.Location = new System.Drawing.Point(306, 80);
            this.progressBarMana.Name = "progressBarMana";
            this.progressBarMana.Size = new System.Drawing.Size(100, 23);
            this.progressBarMana.TabIndex = 12;
            // 
            // progressBarStamina
            // 
            this.progressBarStamina.ForeColor = System.Drawing.Color.Yellow;
            this.progressBarStamina.Location = new System.Drawing.Point(306, 51);
            this.progressBarStamina.Name = "progressBarStamina";
            this.progressBarStamina.Size = new System.Drawing.Size(100, 23);
            this.progressBarStamina.TabIndex = 11;
            // 
            // progressBarLife
            // 
            this.progressBarLife.ForeColor = System.Drawing.Color.Red;
            this.progressBarLife.Location = new System.Drawing.Point(306, 22);
            this.progressBarLife.Name = "progressBarLife";
            this.progressBarLife.Size = new System.Drawing.Size(100, 23);
            this.progressBarLife.TabIndex = 10;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Century Schoolbook", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(211, 84);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(56, 19);
            this.label21.TabIndex = 9;
            this.label21.Text = "Mana:";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Century Schoolbook", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(211, 55);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(88, 19);
            this.label20.TabIndex = 8;
            this.label20.Text = "Ausdauer:";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Century Schoolbook", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(211, 26);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(62, 19);
            this.label19.TabIndex = 7;
            this.label19.Text = "Leben:";
            // 
            // labelLevel
            // 
            this.labelLevel.AutoSize = true;
            this.labelLevel.Font = new System.Drawing.Font("Century Schoolbook", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelLevel.Location = new System.Drawing.Point(70, 85);
            this.labelLevel.Name = "labelLevel";
            this.labelLevel.Size = new System.Drawing.Size(18, 19);
            this.labelLevel.TabIndex = 6;
            this.labelLevel.Text = "1";
            // 
            // labelKlasse
            // 
            this.labelKlasse.AutoSize = true;
            this.labelKlasse.Font = new System.Drawing.Font("Century Schoolbook", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelKlasse.Location = new System.Drawing.Point(70, 55);
            this.labelKlasse.Name = "labelKlasse";
            this.labelKlasse.Size = new System.Drawing.Size(69, 19);
            this.labelKlasse.TabIndex = 5;
            this.labelKlasse.Text = "Krieger";
            // 
            // labeHeroName
            // 
            this.labeHeroName.AutoSize = true;
            this.labeHeroName.Font = new System.Drawing.Font("Century Schoolbook", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labeHeroName.Location = new System.Drawing.Point(70, 26);
            this.labeHeroName.Name = "labeHeroName";
            this.labeHeroName.Size = new System.Drawing.Size(135, 19);
            this.labeHeroName.TabIndex = 4;
            this.labeHeroName.Text = "Max Musterman";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Century Schoolbook", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(6, 85);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(56, 19);
            this.label18.TabIndex = 3;
            this.label18.Text = "Level:";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Century Schoolbook", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(6, 55);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(64, 19);
            this.label17.TabIndex = 2;
            this.label17.Text = "Klasse:";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Century Schoolbook", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(6, 26);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(58, 19);
            this.label16.TabIndex = 1;
            this.label16.Text = "Name:";
            // 
            // lblGold
            // 
            this.lblGold.AutoSize = true;
            this.lblGold.Font = new System.Drawing.Font("Century Schoolbook", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGold.Location = new System.Drawing.Point(70, 112);
            this.lblGold.Name = "lblGold";
            this.lblGold.Size = new System.Drawing.Size(36, 19);
            this.lblGold.TabIndex = 25;
            this.lblGold.Text = "999";
            // 
            // pictureBox2
            // 
            this.pictureBox2.Location = new System.Drawing.Point(696, 36);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(100, 50);
            this.pictureBox2.TabIndex = 5;
            this.pictureBox2.TabStop = false;
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(696, 7);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(100, 23);
            this.progressBar1.TabIndex = 6;
            // 
            // tabPage6
            // 
            this.tabPage6.Location = new System.Drawing.Point(4, 22);
            this.tabPage6.Name = "tabPage6";
            this.tabPage6.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage6.Size = new System.Drawing.Size(802, 337);
            this.tabPage6.TabIndex = 2;
            this.tabPage6.Text = "tabPage6";
            this.tabPage6.UseVisualStyleBackColor = true;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(835, 537);
            this.Controls.Add(this.tabControl_Invisible1);
            this.Name = "MainForm";
            this.Text = "Adventure Game";
            this.tabControl_Invisible1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.tabPage1.ResumeLayout(false);
            this.tabPage3.ResumeLayout(false);
            this.tabControl_Invisible2.ResumeLayout(false);
            this.tabPage4.ResumeLayout(false);
            this.tabPage5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private TabControl_Invisible tabControl_Invisible1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label labelLevel;
        private System.Windows.Forms.Label labelKlasse;
        private System.Windows.Forms.Label labeHeroName;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.ProgressBar progressBarMana;
        private System.Windows.Forms.ProgressBar progressBarStamina;
        private System.Windows.Forms.ProgressBar progressBarLife;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label labelManaDisplay;
        private System.Windows.Forms.Label labelStaminaDisplay;
        private System.Windows.Forms.Label labelLebenDisplay;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label labelSonstiges;
        private System.Windows.Forms.Label labelWaffe;
        private System.Windows.Forms.Label labelRüstung;
        private Class_Form.Create_Character create_Character1;
        private Class_Form.map map1;
        private TabControl_Invisible tabControl_Invisible2;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button cmdItemshop;
        private System.Windows.Forms.Button cmdManatrank;
        private System.Windows.Forms.Button cmdLebenstrank;
        private System.Windows.Forms.Button cmdAngriff;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Label labelGold;
        private System.Windows.Forms.Label lblGold;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.TabPage tabPage6;
    }
}