﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Adventure_Game_Form
{
    public partial class TestForm : Form
    {
        Hero Hans;

        public TestForm()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            Hans = new Hero(1, 1, 1, 1, 1,
            1, 1, 1, 1, "Hans Peter", 1,
            1, 1, 1,0);

            txtName.Text        =   Hans.name_points;
            txtHP.Text          =   Hans.health_points.ToString();
            txtMana.Text        =   Hans.mana_points.ToString();
            txtAusdauer.Text    =   Hans.stamina_points.ToString();

        }

        private void txtName_TextChanged(object sender, EventArgs e)
        {
            Hans.name_points = txtName.Text;
        }

        private void txtHP_TextChanged(object sender, EventArgs e)
        {
            Hans.health_points = Convert.ToInt16(txtHP.Text);
        }
    }
}
