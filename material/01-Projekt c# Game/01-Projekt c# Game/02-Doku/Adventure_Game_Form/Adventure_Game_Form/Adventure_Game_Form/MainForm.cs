﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices;

namespace Adventure_Game_Form
{

    public partial class MainForm : Form
    {

        #region Inizalisierung
        public MainForm()
        {
            InitializeComponent();
        }

        #endregion

        #region Klassen Ausrüstung usw.

        //HeldenKlasse
        Hero Player_Held; 
        
        //--------------------------
        //AUSRÜSTUNG RÜSTUNG
        //--------------------------
        Armor SchwereRüstung;
        Armor LederRüstung ;
        Armor Robe;
        
        //--------------------------
        //AUSRÜSTUNG Waffen
        //--------------------------
        Weapon KlingederVerzauberungdesgestürtztenKönigs;
        Weapon Axt ;
        Weapon Bogen ;
        Weapon StabderWahrheit;

        //--------------------------
        //KARTE
        //--------------------------
        int[,] map = new int[25, 11];

        #endregion

        #region GameFunktions
        
        public void FormRefresher()
        {
            labeHeroName.Text = Player_Held.name_points;
            labelRüstung.Text = Player_Held.Used_Armor_points.name_points;
            labelLevel.Text = Convert.ToString(Player_Held.level_points);
            labelWaffe.Text = Player_Held.Used_Weapon_points.name_points;

            // ProgressBar Leben
            progressBarLife.Maximum = Player_Held.max_hp_points;
            progressBarLife.Value = Player_Held.health_points;

            // ProgressBar Stamina
            progressBarStamina.Maximum = Player_Held.max_stamina_points;
            progressBarStamina.Value = Player_Held.stamina_points;

            //ProgressBar Mana
            progressBarMana.Maximum = Player_Held.max_mana_points;
            progressBarMana.Value = Player_Held.mana_points;

            //Label Leben
            labelLebenDisplay.Text = Convert.ToString(Player_Held.health_points) + "/" + Convert.ToString(Player_Held.max_hp_points);

            //Label Stamina
            labelStaminaDisplay.Text = Convert.ToString(Player_Held.stamina_points) + "/" + Convert.ToString(Player_Held.max_stamina_points);

            //Label Mana
            labelManaDisplay.Text = Convert.ToString(Player_Held.mana_points) + "/" + Convert.ToString(Player_Held.max_mana_points);

            //Label Gold
            lblGold.Text = Convert.ToString(Player_Held.gold_points);
        }

        public void StartGame()
        {
            // Erstellt Armor
            SchwereRüstung = new Armor("Schwere Rüstung", 12, 5, -10);
            LederRüstung = new Armor("Leder Rüstung", 7, 3, -2);
            Robe = new Armor("Robe", 5, 12, 10);

            // Erstellt Waffen
            KlingederVerzauberungdesgestürtztenKönigs = new Weapon("Klinge der Verzauberung des gestürtzten Königs", 15, 6, 1);
            Axt = new Weapon("Axt", 23, 0, 1);
            Bogen = new Weapon("Bogen", 12, 0, 5);
            StabderWahrheit = new Weapon("Stab der Wahrheit", 1, 22, 3);

            // Erstellt den Helden
            Player_Held = new Hero(Convert.ToInt16(create_Character1.Get_Angriff), Convert.ToInt16(create_Character1.Get_Agility),
                Convert.ToInt16(create_Character1.Get_Agility), Convert.ToInt16(create_Character1.Get_Verteidigung),
                200, Convert.ToInt16(create_Character1.Get_Lebenspunkte), 1,
                Convert.ToInt16(create_Character1.Get_Mana), Convert.ToInt16(create_Character1.Get_Magieverteidigung),
                create_Character1.Get_Name, 0, 0, Convert.ToInt16(create_Character1.Get_Stärke), 0, create_Character1.Get_SelectedIndex);

            // Weist dem Spieler der Klasse ensprechened Standarts zu
            #region DefaultEquipment
            switch (Player_Held.class_id_points)
            {
                case 0:
                    Player_Held.Used_Weapon_points = KlingederVerzauberungdesgestürtztenKönigs;
                    Player_Held.Used_Armor_points = SchwereRüstung;
                    break;
                case 1:
                    Player_Held.Used_Weapon_points = StabderWahrheit;
                    Player_Held.Used_Armor_points = Robe;
                    break;
                case 2:
                    Player_Held.Used_Weapon_points = Bogen;
                    Player_Held.Used_Armor_points = LederRüstung;
                    break;
                case 3:
                    Player_Held.Used_Weapon_points = Axt;
                    Player_Held.Used_Armor_points = LederRüstung;
                    break;
                default:
                    break;

            }
            #endregion

            map[5, 8] = 1;
            map1.DrawMap(map, Player_Held.position_x_points, Player_Held.position_y_points, Player_Held.class_id_points);

        }

        #endregion

        #region ButtonEvents

        //Wechselt in den Charakter erstellungs Tab
        private void button5_Click(object sender, EventArgs e)
        {
            tabControl_Invisible1.SelectTab(1);
        }

        //Beendet das Programm
        private void button3_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        
        //Startet das Spiel
        private void button1_Click(object sender, EventArgs e)
        {
            // Startet das Spiel
            StartGame();
            // Wechselt in den SpielTab
            tabControl_Invisible1.SelectTab(2);
            //Refresht die Form zum ersten mal
            FormRefresher();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            tabControl_Invisible2.SelectTab(1);
        }

        //Kampf-Modus Button
        private void cmdAngriff_Click(object sender, EventArgs e)
        {
            Button_ausschalten();
        }
        //Kampf-Modus Button
        private void cmdLebenstrank_Click(object sender, EventArgs e)
        {
            Button_ausschalten();
        }
        //Kampf-Modus Button
        private void cmdManatrank_Click(object sender, EventArgs e)
        {
            Button_ausschalten();
        }
        //Kampf-Modus Button
        private void cmdItemshop_Click(object sender, EventArgs e)
        {
            Button_ausschalten();
        }
        #endregion

        private void map1_CharacterMoved(object sender, EventArgs e)
        {
            Player_Held.position_x_points = map1.Get_XCordinate;
            Player_Held.position_y_points = map1.Get_YCordinate;

        }
        //Testbutton zum anschalten der gesperrten Buttons
        private void button7_Click(object sender, EventArgs e)
        {
            Button_anschalten();
        }

        private void Button_ausschalten()
        {
            cmdAngriff.Enabled = false;
            cmdItemshop.Enabled = false;
            cmdLebenstrank.Enabled = false;
            cmdManatrank.Enabled = false;
        }

        private void Button_anschalten()
        {
            cmdAngriff.Enabled = true;
            cmdItemshop.Enabled = true;
            cmdLebenstrank.Enabled = true;
            cmdManatrank.Enabled = true;
        }       

        #region CustomEvents

        #endregion

    }
}
