﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Adventure_Game_Form
{
    class Weapon: Equipment
    {
        public Weapon(string Name, int Angriff, int MagieAngriff, int range)
        {
            this.name_points = Name;
            this.ap_points = MagieAngriff;
            this.ad_points = Angriff;
            this.range_points = range;
        }
    }
}
