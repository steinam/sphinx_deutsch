﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Adventure_Game_Form
{
    class Equipment
    {
        #region privateVar
        private string name;
      

        /// <summary>
        /// attack Damage
        /// </summary>
        /// <remarks>Angriffsschaden</remarks>
        private int ad;
       
        /// <summary>
        /// Ability Power
        /// </summary>
        private int ap;
        

        /// <summary>
        /// defense
        /// </summary>
        private int def;
      

        /// <summary>
        /// MagieDefense
        /// </summary>
        private int mdef;
       

        /// <summary>
        /// Reichweite
        /// </summary>
        private int range;
       
        #endregion

        #region public
        public string name_points
        {
            get { return name; }
            set { name = value; }
        }

        /// <summary>
        /// attack Damage
        /// </summary>
        /// <remarks>Angriffsschaden</remarks>
        public int ad_points
        {
            get { return ad; }
            set { ad = value; }
        }

        /// <summary>
        /// Geschicklichkeit
        /// </summary>
        public int agility_points
        {
            get { return ad; }
            set { ad = value; }
        }

        /// <summary>
        /// Ability Power
        /// </summary>
        public int ap_points
        {
            get { return ap; }
            set { ap = value; }
        }

        /// <summary>
        /// defense
        /// </summary>
        public int def_points
        {
            get { return def; }
            set { def = value; }
        }

        /// <summary>
        /// MagieDefense
        /// </summary>
        public int mdef_points
        {
            get { return mdef; }
            set { mdef = value; }
        }

        /// <summary>
        /// Reichweite
        /// </summary>
        public int range_points
        {
            get { return range; }
            set { range = value; }
        }
        #endregion
    }

}

