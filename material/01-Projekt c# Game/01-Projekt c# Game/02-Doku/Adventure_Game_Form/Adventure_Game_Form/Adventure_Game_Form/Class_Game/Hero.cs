﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Adventure_Game_Form
{
    class Hero
    {
        #region Konstruktor
        public Hero(int ad, int agility, int ap, int def, int gold, 
            int hp, int level, int mana, int mdef, string name, int range, 
            int skilpoints, int stamina, int xp, int class_id)
        {
            this.ad = ad;
            this.agility = agility;
            this.ap = ap;
            this.def = def;
            this.gold = gold;
            this.max_hp = hp;
            this.hp = hp;
            this.level = level;
            this.max_mana = mana;
            this.mana = mana;
            this.mdef = mdef;
            this.name = name;
            this.range = range;
            this.skillpoints = skilpoints;
            this.max_stamina = stamina;
            this.stamina = stamina;
            this.xp = xp;
            this.class_id = class_id;

        }

        public Hero()
        {
        }

        #endregion

        #region privateVar

        private Armor Used_Armor;
        private Weapon Used_Weapon;


        /// <summary>
        /// Healthpoints
        /// </summary>
        /// <remarks>Healthpoints des Heros</remarks>
        private int hp;
       

        /// <summary>
        /// attack Damage
        /// </summary>
        /// <remarks>Angriffsschaden</remarks>
        private int ad;
       

        /// <summary>
        /// defense
        /// </summary>
        private int def;
       

        /// <summary>
        /// Ability Power
        /// </summary>
        private int ap;
       

        /// <summary>
        /// MagieDefense
        /// </summary>
        private int mdef;
        

        /// <summary>
        /// stamina
        /// </summary>
        private int stamina;
        

        /// <summary>
        /// mana
        /// </summary>
        private int mana;
      

        /// <summary>
        /// Geschicklichkeit
        /// </summary>
        private int agility;
      

        /// <summary>
        /// Reichweite
        /// </summary>
        private int range;
     

        /// <summary>
        /// Name
        /// </summary>
        private string name;
      

        /// <summary>
        /// Erfahrungs Punkte
        /// </summary>
        private int xp;
        

        /// <summary>
        /// Level
        /// </summary>
        private int level;
        

        /// <summary>
        /// Währung
        /// </summary>
        private int gold;
        

        /// <summary>
        /// x-Position
        /// </summary>
        private int position_x;
        

        /// <summary>
        /// y-Position
        /// </summary>
        private int position_y;
        

        /// <summary>
        /// verteilbareAttributpunkte
        /// </summary>
        private int skillpoints;

        private int max_hp;

        private int max_mana;

        private int max_stamina;

        /// <summary>
        /// attack Damage
        /// </summary>
        /// <remarks>Angriffsschaden</remarks>
        private int class_id;
       
#endregion

        #region getter / setter
        public int health_points
        {
            get { return hp; }
            set 
            { hp = value; 
              if (hp <= 0)
              {
                  MessageBox.Show("Du bist toooooooood");
              }
            
            }
        }

        /// <summary>
        /// attack Damage
        /// </summary>
        /// <remarks>Angriffsschaden</remarks>
        public int ad_points
        {
            get { return ad; }
            set { ad = value; }
        }

        /// <summary>
        /// defense
        /// </summary>
        public int def_points
        {
            get { return def; }
            set { def = value; }
        }

        /// <summary>
        /// Ability Power
        /// </summary>
        public int ap_points
        {
            get { return ap; }
            set { ap = value; }
        }

        /// <summary>
        /// MagieDefense
        /// </summary>
        public int mdef_points
        {
            get { return mdef; }
            set { mdef = value; }
        }

        /// <summary>
        /// stamina
        /// </summary>
        public int stamina_points
        {
            get { return stamina; }
            set { stamina = value; }
        }

        /// <summary>
        /// mana
        /// </summary>
        public int mana_points
        {
            get { return mana; }
            set { mana = value; }
        }

        /// <summary>
        /// Geschicklichkeit
        /// </summary>
        public int agility_points
        {
            get { return agility; }
            set { agility = value; }
        }

        /// <summary>
        /// Reichweite
        /// </summary>
        public int range_points
        {
            get { return range; }
            set { range = value; }
        }

        /// <summary>
        /// Erfahrungs Punkte
        /// </summary>
        public int xp_points
        {
            get { return xp; }
            set { xp = value; }
        }

        /// <summary>
        /// Level
        /// </summary>
        public int level_points
        {
            get { return level; }
            set { level = value; }
        }

        /// <summary>
        /// Währung
        /// </summary>
        public int gold_points
        {
            get { return gold; }
            set { gold = value; }
        }

        /// <summary>
        /// x-Position
        /// </summary>
        public int position_x_points
        {
            get { return position_x; }
            set { position_x = value; }
        }

        /// <summary>
        /// y-Position
        /// </summary>
        public int position_y_points
        {
            get { return position_y; }
            set { position_y = value; }
        }

        /// <summary>
        /// verteilbareAttributpunkte
        /// </summary>
        public int skillpoints_points
        {
            get { return skillpoints; }
            set { skillpoints = value; }
        }

        /// <summary>
        /// verteilbareAttributpunkte
        /// </summary>
        public string name_points
        {
            get { return name; }
            set { name = value; }
        }
       

        public int max_hp_points
        {
            get { return max_hp; }
            set { max_hp = value; }
        }

        public int max_mana_points
        {
            get { return max_mana; }
            set { max_mana = value; }
        }

        public int max_stamina_points
        {
            get { return max_stamina; }
            set { max_stamina = value; }
        }

        /// <summary>
        /// attack Damage
        /// </summary>
        /// <remarks>Angriffsschaden</remarks>
        public int class_id_points
        {
            get { return class_id; }
            set { class_id = value; }
        }

        public Armor Used_Armor_points
        {
            get
            {
                return this.Used_Armor;
            }
            set
            {
                unuse_armor(this.Used_Armor);
                use_armor(value);
            }
        }

        public Weapon Used_Weapon_points
        {
            get
            {
                return this.Used_Weapon;
            }
            set
            {
                unuse_weapon(this.Used_Weapon);
                use_weapon(value);
            }
        }
       
        #endregion

        #region functions


        /// <summary>
        /// Level aufstieg
        /// </summary>
        private void level_up()
        {
            throw new System.NotImplementedException();
        }

        public int attack()
        {
            throw new System.NotImplementedException();
        }

        public int defense()
        {
            throw new System.NotImplementedException();
        }

        public int m_attack()
        {
            throw new System.NotImplementedException();
        }

        public int m_defense()
        {
            throw new System.NotImplementedException();
        }

        private void use_armor(Armor used_Armor)
        {
            this.def = this.def + used_Armor.def_points;
            this.mdef = this.mdef + used_Armor.mdef_points;
            this.agility = this.agility + used_Armor.agility_points;
            this.Used_Armor = used_Armor;
        }

        private void use_weapon(Weapon used_weapon)
        {
            this.ad = this.ad + used_weapon.ad_points;
            this.range = used_weapon.range_points;
            this.ap = this.ap + used_weapon.ap_points;
            this.Used_Weapon = used_weapon;
        }

        private void unuse_armor(Armor unused_Armor)
        {
            if (this.Used_Armor == null) return;
            this.def = this.def - unused_Armor.def_points;
            this.mdef = this.mdef - unused_Armor.mdef_points;
            this.agility = this.agility - unused_Armor.agility_points;
        }

        private void unuse_weapon(Weapon unused_weapon)
        {
            if (this.Used_Weapon == null) return;
            this.ad = this.ad - unused_weapon.ad_points;
            this.range = 0;
            this.ap = this.ap - unused_weapon.ap_points;
        }


    }
        #endregion
}
       