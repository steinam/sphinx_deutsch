﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Adventure_Game_Form
{
    class Armor : Equipment
    {
        public Armor(string name, int Verteidigung, int MagieVerteidigung, int Agility)
        {
            this.name_points = name;
            this.def_points = Verteidigung;
            this.mdef_points = MagieVerteidigung;
            this.agility_points = Agility;

         }
    }
}
