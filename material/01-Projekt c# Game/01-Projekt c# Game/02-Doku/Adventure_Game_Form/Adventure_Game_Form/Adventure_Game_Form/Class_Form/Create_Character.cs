﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Adventure_Game_Form.Class_Form
{
    public partial class Create_Character : UserControl
    {
        #region Inizalisierung
        public Create_Character()
        {
            InitializeComponent();
        }

        private void Create_Character_Load(object sender, EventArgs e)
        {
            pictureBox1.Image = imageList1.Images[0];
            comboBox1.SelectedIndex = 0;
        }
        #endregion

        #region Variablen
        private int _remainingPoints = 30;
        private decimal[] tempValue = new decimal[8];
        #endregion

        #region Pars ControlValues

        public decimal Get_Angriff
        {
            get {return numericUpDownAngriff.Value; } 
        }
        public decimal Get_Agility
        {
            get { return numericUpDownAgility.Value; } 
        }
        public decimal Get_Abilitypower
        {
            get {return  numericUpDownAbilitypower.Value  ; } 
        }
        public decimal Get_Verteidigung
        {
            get {return numericUpDownVerteidigung.Value ; } 
        }
        public decimal Get_Lebenspunkte
        {
            get {return numericUpDownLebenspunkte.Value  ; } 
        }
        public decimal Get_Mana
        {
            get {return numericUpDownMana.Value ; } 
        }
        public decimal Get_Magieverteidigung
        {
            get {return numericUpDownMagieverteidigung.Value  ; } 
        }
        public string Get_Name
        {
            get {return textBoxName.Text  ; } 
        }
        public decimal Get_Stärke
        {
            get {return numericUpDownStärke.Value  ; } 
        }
        public int Get_SelectedIndex
        {
            get {return  comboBox1.SelectedIndex ; } 
        }
           
        #endregion

        #region CharakterPictureChange

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            pictureBox1.Image = imageList1.Images[comboBox1.SelectedIndex];
        }

        #endregion

        #region RemainingPoints
        private NumericUpDown ChangeValue(NumericUpDown temp, int position)
        {
            if (temp.Value < tempValue[position])
            {
                tempValue[position] = temp.Value;
                _remainingPoints++;
            }
            else if (temp.Value > tempValue[position])
            {
                if (_remainingPoints < 1)
                {
                    temp.Value = tempValue[position];
                    return temp;
                }
                _remainingPoints--;
                tempValue[position] = temp.Value;
            }

            label6.Text = _remainingPoints.ToString();
            return temp;
        }
        #endregion

        private void numericUpDownAngriff_ValueChanged(object sender, EventArgs e)
        {
            numericUpDownAngriff = ChangeValue(numericUpDownAngriff, 0);
        }

        private void numericUpDownAgility_ValueChanged(object sender, EventArgs e)
        {
            numericUpDownAgility = ChangeValue(numericUpDownAgility, 1);
        }

        private void numericUpDownAbilitypower_ValueChanged(object sender, EventArgs e)
        {
            numericUpDownAbilitypower = ChangeValue(numericUpDownAbilitypower, 2);
        }

        private void numericUpDownVerteidigung_ValueChanged(object sender, EventArgs e)
        {
            numericUpDownVerteidigung = ChangeValue(numericUpDownVerteidigung, 3);
        }

        private void numericUpDownMana_ValueChanged(object sender, EventArgs e)
        {
            numericUpDownMana = ChangeValue(numericUpDownMana, 4);
        }

        private void numericUpDownLebenspunkte_ValueChanged(object sender, EventArgs e)
        {
            numericUpDownLebenspunkte = ChangeValue(numericUpDownLebenspunkte, 5);
        }

        private void numericUpDownMagieverteidigung_ValueChanged(object sender, EventArgs e)
        {
            numericUpDownMagieverteidigung = ChangeValue(numericUpDownMagieverteidigung, 6);
        }

        private void numericUpDownStärke_ValueChanged(object sender, EventArgs e)
        {
            numericUpDownStärke = ChangeValue(numericUpDownStärke, 7);
        }

    }
}
