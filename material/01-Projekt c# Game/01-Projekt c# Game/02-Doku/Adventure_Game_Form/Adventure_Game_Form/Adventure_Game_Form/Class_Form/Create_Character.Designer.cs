﻿namespace Adventure_Game_Form.Class_Form
{
    partial class Create_Character
    {
        /// <summary> 
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Komponenten-Designer generierter Code

        /// <summary> 
        /// Erforderliche Methode für die Designerunterstützung. 
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Create_Character));
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.numericUpDownStärke = new System.Windows.Forms.NumericUpDown();
            this.numericUpDownMagieverteidigung = new System.Windows.Forms.NumericUpDown();
            this.numericUpDownLebenspunkte = new System.Windows.Forms.NumericUpDown();
            this.numericUpDownMana = new System.Windows.Forms.NumericUpDown();
            this.numericUpDownVerteidigung = new System.Windows.Forms.NumericUpDown();
            this.numericUpDownAbilitypower = new System.Windows.Forms.NumericUpDown();
            this.numericUpDownAgility = new System.Windows.Forms.NumericUpDown();
            this.label14 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.numericUpDownAngriff = new System.Windows.Forms.NumericUpDown();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label15 = new System.Windows.Forms.Label();
            this.textBoxName = new System.Windows.Forms.TextBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownStärke)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownMagieverteidigung)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownLebenspunkte)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownMana)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownVerteidigung)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownAbilitypower)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownAgility)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownAngriff)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.numericUpDownStärke);
            this.groupBox2.Controls.Add(this.numericUpDownMagieverteidigung);
            this.groupBox2.Controls.Add(this.numericUpDownLebenspunkte);
            this.groupBox2.Controls.Add(this.numericUpDownMana);
            this.groupBox2.Controls.Add(this.numericUpDownVerteidigung);
            this.groupBox2.Controls.Add(this.numericUpDownAbilitypower);
            this.groupBox2.Controls.Add(this.numericUpDownAgility);
            this.groupBox2.Controls.Add(this.label14);
            this.groupBox2.Controls.Add(this.label12);
            this.groupBox2.Controls.Add(this.label11);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.numericUpDownAngriff);
            this.groupBox2.Font = new System.Drawing.Font("Century Schoolbook", 14.25F, System.Drawing.FontStyle.Bold);
            this.groupBox2.Location = new System.Drawing.Point(249, 44);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(540, 394);
            this.groupBox2.TabIndex = 10;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Attribute";
            // 
            // numericUpDownStärke
            // 
            this.numericUpDownStärke.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.numericUpDownStärke.Font = new System.Drawing.Font("Century Schoolbook", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numericUpDownStärke.Location = new System.Drawing.Point(240, 249);
            this.numericUpDownStärke.Name = "numericUpDownStärke";
            this.numericUpDownStärke.ReadOnly = true;
            this.numericUpDownStärke.Size = new System.Drawing.Size(290, 26);
            this.numericUpDownStärke.TabIndex = 21;
            this.numericUpDownStärke.ValueChanged += new System.EventHandler(this.numericUpDownStärke_ValueChanged);
            // 
            // numericUpDownMagieverteidigung
            // 
            this.numericUpDownMagieverteidigung.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.numericUpDownMagieverteidigung.Font = new System.Drawing.Font("Century Schoolbook", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numericUpDownMagieverteidigung.Location = new System.Drawing.Point(240, 217);
            this.numericUpDownMagieverteidigung.Name = "numericUpDownMagieverteidigung";
            this.numericUpDownMagieverteidigung.ReadOnly = true;
            this.numericUpDownMagieverteidigung.Size = new System.Drawing.Size(290, 26);
            this.numericUpDownMagieverteidigung.TabIndex = 19;
            this.numericUpDownMagieverteidigung.ValueChanged += new System.EventHandler(this.numericUpDownMagieverteidigung_ValueChanged);
            // 
            // numericUpDownLebenspunkte
            // 
            this.numericUpDownLebenspunkte.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.numericUpDownLebenspunkte.Font = new System.Drawing.Font("Century Schoolbook", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numericUpDownLebenspunkte.Location = new System.Drawing.Point(240, 185);
            this.numericUpDownLebenspunkte.Name = "numericUpDownLebenspunkte";
            this.numericUpDownLebenspunkte.ReadOnly = true;
            this.numericUpDownLebenspunkte.Size = new System.Drawing.Size(290, 26);
            this.numericUpDownLebenspunkte.TabIndex = 18;
            this.numericUpDownLebenspunkte.ValueChanged += new System.EventHandler(this.numericUpDownLebenspunkte_ValueChanged);
            // 
            // numericUpDownMana
            // 
            this.numericUpDownMana.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.numericUpDownMana.Font = new System.Drawing.Font("Century Schoolbook", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numericUpDownMana.Location = new System.Drawing.Point(240, 153);
            this.numericUpDownMana.Name = "numericUpDownMana";
            this.numericUpDownMana.ReadOnly = true;
            this.numericUpDownMana.Size = new System.Drawing.Size(290, 26);
            this.numericUpDownMana.TabIndex = 17;
            this.numericUpDownMana.ValueChanged += new System.EventHandler(this.numericUpDownMana_ValueChanged);
            // 
            // numericUpDownVerteidigung
            // 
            this.numericUpDownVerteidigung.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.numericUpDownVerteidigung.Font = new System.Drawing.Font("Century Schoolbook", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numericUpDownVerteidigung.Location = new System.Drawing.Point(240, 121);
            this.numericUpDownVerteidigung.Name = "numericUpDownVerteidigung";
            this.numericUpDownVerteidigung.ReadOnly = true;
            this.numericUpDownVerteidigung.Size = new System.Drawing.Size(290, 26);
            this.numericUpDownVerteidigung.TabIndex = 16;
            this.numericUpDownVerteidigung.ValueChanged += new System.EventHandler(this.numericUpDownVerteidigung_ValueChanged);
            // 
            // numericUpDownAbilitypower
            // 
            this.numericUpDownAbilitypower.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.numericUpDownAbilitypower.Font = new System.Drawing.Font("Century Schoolbook", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numericUpDownAbilitypower.Location = new System.Drawing.Point(240, 89);
            this.numericUpDownAbilitypower.Name = "numericUpDownAbilitypower";
            this.numericUpDownAbilitypower.ReadOnly = true;
            this.numericUpDownAbilitypower.Size = new System.Drawing.Size(290, 26);
            this.numericUpDownAbilitypower.TabIndex = 15;
            this.numericUpDownAbilitypower.ValueChanged += new System.EventHandler(this.numericUpDownAbilitypower_ValueChanged);
            // 
            // numericUpDownAgility
            // 
            this.numericUpDownAgility.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.numericUpDownAgility.Font = new System.Drawing.Font("Century Schoolbook", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numericUpDownAgility.Location = new System.Drawing.Point(240, 57);
            this.numericUpDownAgility.Name = "numericUpDownAgility";
            this.numericUpDownAgility.ReadOnly = true;
            this.numericUpDownAgility.Size = new System.Drawing.Size(290, 26);
            this.numericUpDownAgility.TabIndex = 14;
            this.numericUpDownAgility.ValueChanged += new System.EventHandler(this.numericUpDownAgility_ValueChanged);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Century Schoolbook", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(13, 251);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(61, 19);
            this.label14.TabIndex = 13;
            this.label14.Text = "Stärke";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Century Schoolbook", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(10, 219);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(155, 19);
            this.label12.TabIndex = 11;
            this.label12.Text = "Magieverteidigung";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Century Schoolbook", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(10, 187);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(121, 19);
            this.label11.TabIndex = 10;
            this.label11.Text = "Lebenspunkte";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Century Schoolbook", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(10, 155);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(52, 19);
            this.label10.TabIndex = 9;
            this.label10.Text = "Mana";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Century Schoolbook", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(10, 123);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(110, 19);
            this.label9.TabIndex = 8;
            this.label9.Text = "Verteidigung";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Century Schoolbook", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(10, 91);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(109, 19);
            this.label8.TabIndex = 7;
            this.label8.Text = "Abilitypower";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Century Schoolbook", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(10, 59);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(59, 19);
            this.label7.TabIndex = 6;
            this.label7.Text = "Agility";
            // 
            // label6
            // 
            this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Century Schoolbook", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(503, 372);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(27, 19);
            this.label6.TabIndex = 5;
            this.label6.Text = "30";
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Century Schoolbook", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(323, 372);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(174, 19);
            this.label5.TabIndex = 4;
            this.label5.Text = "Verbleibende Punkte";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Century Schoolbook", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(10, 29);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(64, 19);
            this.label2.TabIndex = 0;
            this.label2.Text = "Angriff";
            // 
            // numericUpDownAngriff
            // 
            this.numericUpDownAngriff.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.numericUpDownAngriff.Font = new System.Drawing.Font("Century Schoolbook", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numericUpDownAngriff.Location = new System.Drawing.Point(240, 27);
            this.numericUpDownAngriff.Name = "numericUpDownAngriff";
            this.numericUpDownAngriff.ReadOnly = true;
            this.numericUpDownAngriff.Size = new System.Drawing.Size(290, 26);
            this.numericUpDownAngriff.TabIndex = 3;
            this.numericUpDownAngriff.ValueChanged += new System.EventHandler(this.numericUpDownAngriff_ValueChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.groupBox1.Controls.Add(this.label15);
            this.groupBox1.Controls.Add(this.textBoxName);
            this.groupBox1.Controls.Add(this.pictureBox1);
            this.groupBox1.Controls.Add(this.comboBox1);
            this.groupBox1.Font = new System.Drawing.Font("Century Schoolbook", 14.25F, System.Drawing.FontStyle.Bold);
            this.groupBox1.Location = new System.Drawing.Point(0, 44);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(246, 394);
            this.groupBox1.TabIndex = 9;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Klasse";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Century Schoolbook", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(6, 29);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(72, 23);
            this.label15.TabIndex = 6;
            this.label15.Text = "Name:";
            // 
            // textBoxName
            // 
            this.textBoxName.Location = new System.Drawing.Point(84, 26);
            this.textBoxName.Name = "textBoxName";
            this.textBoxName.Size = new System.Drawing.Size(156, 30);
            this.textBoxName.TabIndex = 5;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(6, 62);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(234, 289);
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            // 
            // comboBox1
            // 
            this.comboBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "Krieger",
            "Magier",
            "Waldläufer",
            "Zwerg"});
            this.comboBox1.Location = new System.Drawing.Point(6, 357);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(234, 31);
            this.comboBox1.TabIndex = 4;
            this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Century Schoolbook", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(-5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(274, 30);
            this.label1.TabIndex = 8;
            this.label1.Text = "Charakter erstellung";
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "profession_human_warrior.png");
            this.imageList1.Images.SetKeyName(1, "profession_human_mage.png");
            this.imageList1.Images.SetKeyName(2, "profession_elf_ranger.png");
            this.imageList1.Images.SetKeyName(3, "profession_dwarf_mercenary.png");
            // 
            // Create_Character
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label1);
            this.Name = "Create_Character";
            this.Size = new System.Drawing.Size(816, 452);
            this.Load += new System.EventHandler(this.Create_Character_Load);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownStärke)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownMagieverteidigung)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownLebenspunkte)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownMana)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownVerteidigung)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownAbilitypower)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownAgility)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownAngriff)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.NumericUpDown numericUpDownStärke;
        private System.Windows.Forms.NumericUpDown numericUpDownMagieverteidigung;
        private System.Windows.Forms.NumericUpDown numericUpDownLebenspunkte;
        private System.Windows.Forms.NumericUpDown numericUpDownMana;
        private System.Windows.Forms.NumericUpDown numericUpDownVerteidigung;
        private System.Windows.Forms.NumericUpDown numericUpDownAbilitypower;
        private System.Windows.Forms.NumericUpDown numericUpDownAgility;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.NumericUpDown numericUpDownAngriff;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox textBoxName;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ImageList imageList1;

    }
}
