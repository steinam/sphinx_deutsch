﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Adventure_Game_Form.Class_Form
{
    public partial class map : UserControl
    {
        #region Inizialisierung
        public map()
        {
            InitializeComponent();
        }
        #endregion

        #region public Objekts
        public CharacterImage[] Characters = new CharacterImage[20];
        public PictureBox HeroCharacter = new PictureBox();
        public event EventHandler CharacterMoved;
        #endregion

        private int XConrdinate, YConrdinate, FieldTyp;

        #region First Map draw
        // Zeichnet die Map aus einem 2 Dimensionalen Array
        public void DrawMap(int[,] tilesMap, int xPosition, int yPosition, int Class)
        {
            #region drawMap
            this.Controls.Clear();
            CustomImage[,] tile = new CustomImage[tilesMap.GetLength(0), tilesMap.GetLength(1)];
            int Xcursor = 0;
            int Ycursor = 0;

            for (int xAxis = 0; xAxis < tilesMap.GetLength(0); xAxis++)
            {
                for (int yAxis = 0; yAxis < tilesMap.GetLength(1); yAxis++)
                {
                    tile[xAxis, yAxis] = new CustomImage();
                    tile[xAxis, yAxis].Image = imageList1.Images[tilesMap[xAxis, yAxis]];
                    tile[xAxis, yAxis].Location = new Point(Xcursor, Ycursor);
                    tile[xAxis, yAxis].Size = new Size(32, 32);
                    tile[xAxis, yAxis].Click += new System.EventHandler(tile_click);
                    tile[xAxis, yAxis].GeländeTyp = tilesMap[xAxis, yAxis];
                    tile[xAxis, yAxis].BorderStyle = BorderStyle.None;
                    tile[xAxis, yAxis].Get_xCord = xAxis;
                    tile[xAxis, yAxis].Get_yCord = yAxis;
                    Ycursor = Ycursor + 32;
                }
                Ycursor = 0;
                Xcursor = Xcursor + 32;
            }

            foreach (CustomImage box in tile)
            {
                this.Controls.Add(box);
            }
            #endregion

            #region Draw Hero
                HeroCharacter.Image = imageList2.Images[Class];
                HeroCharacter.Size = new Size(16, 16);
                HeroCharacter.BackColor = Color.Transparent;
                this.Controls.Add(HeroCharacter);
                HeroCharacter.Parent = tile[xPosition, xPosition];
                HeroCharacter.Location = new Point(8, 8);
                //HeroCharacter.BringToFront();
                
            #endregion

            #region Draw Charakters
            foreach (CharacterImage c in Characters)
            {
                this.Controls.Add(c);
            }
            #endregion

        }
        #endregion

        #region TileCick Event
        // Click Event für die Dynamischen PictureBoxen
        private void tile_click(Object sender, System.EventArgs e)
        {
            CustomImage senderPictureBox = (CustomImage)sender;
            if ((senderPictureBox.GeländeTyp == 1) || (senderPictureBox.GeländeTyp == 4) || (senderPictureBox.GeländeTyp == 6))
                return;
            HeroCharacter.Parent = senderPictureBox;
            XConrdinate = senderPictureBox.Get_xCord;
            YConrdinate = senderPictureBox.Get_yCord;
            FieldTyp = senderPictureBox.GeländeTyp;
            this.CharacterMoved(this, e);
        }
        #endregion

        public int Get_XCordinate
        {
            get { return XConrdinate; }
        }

        public int Get_YCordinate
        {
            get { return YConrdinate; }
        }

        public int Get_FieldTyp
        {
            get { return FieldTyp; }
        }


    }

#region Custom Classes 
    // Custom Classe damit der GeländeTyp mit gespeichert werden kann
    public partial class CustomImage : PictureBox
    {
        private int _geländeTyp, xCord, yCord;
        public int GeländeTyp
        {
            get
            {
                return _geländeTyp;
            }
        
            set
            {
                _geländeTyp = value;
            }

        }

        public int Get_xCord
        {
            get
            {
                return xCord;
            }

            set
            {
                xCord = value;
            }

        }

        public int Get_yCord
        {
            get
            {
                return yCord;
            }

            set
            {
                yCord = value;
            }

        }
      

    }

    // Custom Classe damit der GeländeTyp mit gespeichert werden kann
    public partial class CharacterImage : PictureBox
    {
        private int _Typ;
        public int Typ
        {
            get
            {
                return _Typ;
            }

            set
            {
                _Typ = value;
            }

        }
    }

#endregion

}
