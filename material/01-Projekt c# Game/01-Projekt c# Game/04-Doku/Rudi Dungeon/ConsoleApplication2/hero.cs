﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication2
{
    public class hero
    {
        private decimal _str;
        private decimal _bra;
        private decimal _chr;
        private decimal _cons;
        private decimal _dex;
        private decimal _intl;
        private decimal _hp;
        private decimal _mp;
        private decimal _ini;
        private string _name;
        private string _profession;
        private string _race;
        private int _starpoints;
       
        

        public hero(string name, decimal strenght, decimal dexterity, decimal constitution, decimal bravery, decimal charisma, decimal intelligence, string health, string mana, string intuition, string profession, string race)
        {
            switch (profession)
            {
                case "Warrior":
                      _str = strenght + 2;
                      _bra = bravery + 1;
                      _chr = charisma + 0;
                      _cons = constitution + 1;
                      _dex = dexterity + 0;
                      _intl = intelligence + 0;
                      _hp = System.Convert.ToDecimal(health) + 5;
                      _mp = System.Convert.ToDecimal(mana) + 0;
                      _ini = System.Convert.ToDecimal(intuition) + 2;
                      _name = name;
                      _profession = profession;
                      _race = race;
                        break;
          
                case "Mage":

                     _str = strenght + 0;
                      _bra = bravery + 0;
                      _chr = charisma + 1;
                      _cons = constitution + 0;
                      _dex = dexterity + 1;
                      _intl = intelligence + 2;
                      _hp = System.Convert.ToDecimal(health) + 2;
                      _mp = System.Convert.ToDecimal(mana) + 5;
                      _ini = System.Convert.ToDecimal(intuition) + 2;
                      _name = name;
                      _profession = profession;
                      _race = race;
                     break;

                case "Archer":

                     _str = strenght + 0;
                      _bra = bravery + 2;
                      _chr = charisma + 1;
                      _cons = constitution + 1;
                      _dex = dexterity + 2;
                      _intl = intelligence + 0;
                      _hp = System.Convert.ToDecimal(health) + 3;
                      _mp = System.Convert.ToDecimal(mana) + 0;
                      _ini = System.Convert.ToDecimal(intuition) + 3;
                      _name = name;
                      _profession = profession;
                      _race = race;
                     break;

                case "Thief":

                     _str = strenght + 0;
                      _bra = bravery + 1;
                      _chr = charisma + 0;
                      _cons = constitution + 1;
                      _dex = dexterity + 2;
                      _intl = intelligence + 1;
                      _hp = System.Convert.ToDecimal(health) + 2;
                      _mp = System.Convert.ToDecimal(mana) + 0;
                      _ini = System.Convert.ToDecimal(intuition) + 3;
                      _name = name;
                      _profession = profession;
                      _race = race;
                     break;

                case "Dodo":

                     _str = strenght + 20;
                      _bra = bravery + 20;
                      _chr = charisma + 20;
                      _cons = constitution + 20;
                      _dex = dexterity + 20;
                      _intl = intelligence + 20;
                      _hp = System.Convert.ToDecimal(health) + 20;
                      _mp = System.Convert.ToDecimal(mana) + 20;
                      _ini = System.Convert.ToDecimal(intuition) + 20;
                      _name = name;
                      _profession = profession;
                      _race = race;
                     break;
            }

          }
    
        public string name
        {
            get
            {
                return _name;
            }
            set
            {
                _name = value;
            }
        }

        /// <remarks>Einstellungspunkte für Charakterwerte</remarks>
        public int starpoints
        {
            get
            {
                return _starpoints;
            }
            set
            {
                _starpoints = value;
            }
        }

        /// <remarks>Rasse</remarks>
        public string race
        {
            get
            {
                return _race;
            }
            set
            {
                _race = value;
            }
        }

        /// <remarks>Klasse</remarks>
        public string profession
        {
            get
            {
                return _profession;
            }
            set
            {
                _profession = value;
            }
        }

        /// <remarks>Lebenspunkte</remarks>
        public decimal hp
        {
            get
            {
                return _hp;
            }
            set
            {
                _hp = value;
            }
        }

        /// <remarks>Magiepunkte</remarks>
        public decimal mp
        {
            get
            {
                return _mp;
            }
            set
            {
                _mp = value;
            }
        }

        /// <remarks>Initiative</remarks>
        public decimal ini
        {
            get
            {
                return _ini;
            }
            set
            {
                _ini = value;
            }
        }

        /// <remarks>Mut</remarks>
        public decimal bra
        {
            get
            {
                return _bra;
            }
            set
            {
                _bra = value;
            }
        }

        /// <remarks>Konstitution</remarks>
        public decimal cons
        {
            get
            {
                return _cons;
            }
            set
            {
                _cons = value;
            }
        }

        /// <remarks>Fingerfertigkeit</remarks>
        public decimal dex
        {
            get
            {
                return _dex;
            }
            set
            {
                _dex = value;
            }
        }

        /// <remarks>Inteligenz</remarks>
        public decimal intl
        {
            get
            {
                return _intl;
            }
            set
            {
                _intl = value;
            }
        }

        /// <remarks>Charisma</remarks>
        public decimal chr
        {
            get
            {
                return _chr;
            }
            set
            {
                _chr = value;
            }
        }

        /// <remarks>Stärke</remarks>
        public decimal str
        {
            get
            {
                return _str;
            }
            set
            {
                _str = value;
            }
        }
    }
}
