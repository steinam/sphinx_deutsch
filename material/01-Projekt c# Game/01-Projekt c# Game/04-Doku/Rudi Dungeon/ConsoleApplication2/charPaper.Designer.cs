﻿namespace ConsoleApplication2
{
    partial class charPaper
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.createButton = new System.Windows.Forms.Button();
            this.professionField = new System.Windows.Forms.ComboBox();
            this.strenghtName = new System.Windows.Forms.Label();
            this.ProfessionName = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.intelligenceName = new System.Windows.Forms.Label();
            this.ConstitutionName = new System.Windows.Forms.Label();
            this.dexterityName = new System.Windows.Forms.Label();
            this.BraveryName = new System.Windows.Forms.Label();
            this.raceName = new System.Windows.Forms.Label();
            this.StartpointsName = new System.Windows.Forms.Label();
            this.HealtName = new System.Windows.Forms.Label();
            this.manaName = new System.Windows.Forms.Label();
            this.IntuitionName = new System.Windows.Forms.Label();
            this.gameName = new System.Windows.Forms.Label();
            this.strenghtField = new System.Windows.Forms.NumericUpDown();
            this.braveryField = new System.Windows.Forms.NumericUpDown();
            this.dexterityField = new System.Windows.Forms.NumericUpDown();
            this.constitutionField = new System.Windows.Forms.NumericUpDown();
            this.intelligenceField = new System.Windows.Forms.NumericUpDown();
            this.charismaField = new System.Windows.Forms.NumericUpDown();
            this.equipmentField = new System.Windows.Forms.TextBox();
            this.nameField = new System.Windows.Forms.TextBox();
            this.startpointsField = new System.Windows.Forms.TextBox();
            this.CharismaName = new System.Windows.Forms.Label();
            this.healthField = new System.Windows.Forms.TextBox();
            this.manaField = new System.Windows.Forms.TextBox();
            this.intuitionField = new System.Windows.Forms.TextBox();
            this.RaceField = new System.Windows.Forms.ComboBox();
            this.resetbutton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.strenghtField)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.braveryField)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dexterityField)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.constitutionField)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.intelligenceField)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.charismaField)).BeginInit();
            this.SuspendLayout();
            // 
            // createButton
            // 
            this.createButton.Location = new System.Drawing.Point(553, 371);
            this.createButton.Name = "createButton";
            this.createButton.Size = new System.Drawing.Size(79, 29);
            this.createButton.TabIndex = 0;
            this.createButton.Text = "Create";
            this.createButton.UseVisualStyleBackColor = true;
            this.createButton.Click += new System.EventHandler(this.createButton_Click);
            // 
            // professionField
            // 
            this.professionField.FormattingEnabled = true;
            this.professionField.Items.AddRange(new object[] {
            "Warrior",
            "Mage",
            "Archer",
            "Thief",
            "Dodo"});
            this.professionField.Location = new System.Drawing.Point(288, 141);
            this.professionField.Name = "professionField";
            this.professionField.Size = new System.Drawing.Size(121, 21);
            this.professionField.TabIndex = 1;
            this.professionField.SelectedIndexChanged += new System.EventHandler(this.professionField_SelectedIndexChanged);
            // 
            // strenghtName
            // 
            this.strenghtName.AutoSize = true;
            this.strenghtName.Location = new System.Drawing.Point(30, 76);
            this.strenghtName.Name = "strenghtName";
            this.strenghtName.Size = new System.Drawing.Size(47, 13);
            this.strenghtName.TabIndex = 2;
            this.strenghtName.Text = "Strenght";
            // 
            // ProfessionName
            // 
            this.ProfessionName.AutoSize = true;
            this.ProfessionName.Location = new System.Drawing.Point(213, 144);
            this.ProfessionName.Name = "ProfessionName";
            this.ProfessionName.Size = new System.Drawing.Size(56, 13);
            this.ProfessionName.TabIndex = 3;
            this.ProfessionName.Text = "Profession";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(35, 315);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(0, 13);
            this.label3.TabIndex = 4;
            this.label3.Click += new System.EventHandler(this.label3_Click);
            // 
            // intelligenceName
            // 
            this.intelligenceName.AutoSize = true;
            this.intelligenceName.Location = new System.Drawing.Point(18, 207);
            this.intelligenceName.Name = "intelligenceName";
            this.intelligenceName.Size = new System.Drawing.Size(61, 13);
            this.intelligenceName.TabIndex = 5;
            this.intelligenceName.Text = "Intelligence";
            // 
            // ConstitutionName
            // 
            this.ConstitutionName.AutoSize = true;
            this.ConstitutionName.Location = new System.Drawing.Point(17, 170);
            this.ConstitutionName.Name = "ConstitutionName";
            this.ConstitutionName.Size = new System.Drawing.Size(62, 13);
            this.ConstitutionName.TabIndex = 6;
            this.ConstitutionName.Text = "Constitution";
            // 
            // dexterityName
            // 
            this.dexterityName.AutoSize = true;
            this.dexterityName.Location = new System.Drawing.Point(30, 136);
            this.dexterityName.Name = "dexterityName";
            this.dexterityName.Size = new System.Drawing.Size(48, 13);
            this.dexterityName.TabIndex = 7;
            this.dexterityName.Text = "Dexterity";
            // 
            // BraveryName
            // 
            this.BraveryName.AutoSize = true;
            this.BraveryName.Location = new System.Drawing.Point(28, 105);
            this.BraveryName.Name = "BraveryName";
            this.BraveryName.Size = new System.Drawing.Size(43, 13);
            this.BraveryName.TabIndex = 8;
            this.BraveryName.Text = "Bravery";
            // 
            // raceName
            // 
            this.raceName.AutoSize = true;
            this.raceName.Location = new System.Drawing.Point(220, 90);
            this.raceName.Name = "raceName";
            this.raceName.Size = new System.Drawing.Size(33, 13);
            this.raceName.TabIndex = 9;
            this.raceName.Text = "Race";
            // 
            // StartpointsName
            // 
            this.StartpointsName.AutoSize = true;
            this.StartpointsName.Location = new System.Drawing.Point(488, 33);
            this.StartpointsName.Name = "StartpointsName";
            this.StartpointsName.Size = new System.Drawing.Size(36, 13);
            this.StartpointsName.TabIndex = 10;
            this.StartpointsName.Text = "Points";
            // 
            // HealtName
            // 
            this.HealtName.AutoSize = true;
            this.HealtName.Location = new System.Drawing.Point(485, 103);
            this.HealtName.Name = "HealtName";
            this.HealtName.Size = new System.Drawing.Size(38, 13);
            this.HealtName.TabIndex = 11;
            this.HealtName.Text = "Health";
            // 
            // manaName
            // 
            this.manaName.AutoSize = true;
            this.manaName.Location = new System.Drawing.Point(485, 134);
            this.manaName.Name = "manaName";
            this.manaName.Size = new System.Drawing.Size(34, 13);
            this.manaName.TabIndex = 12;
            this.manaName.Text = "Mana";
            // 
            // IntuitionName
            // 
            this.IntuitionName.AutoSize = true;
            this.IntuitionName.Location = new System.Drawing.Point(485, 164);
            this.IntuitionName.Name = "IntuitionName";
            this.IntuitionName.Size = new System.Drawing.Size(44, 13);
            this.IntuitionName.TabIndex = 13;
            this.IntuitionName.Text = "Intuition";
            // 
            // gameName
            // 
            this.gameName.AutoSize = true;
            this.gameName.Location = new System.Drawing.Point(294, 9);
            this.gameName.Name = "gameName";
            this.gameName.Size = new System.Drawing.Size(76, 13);
            this.gameName.TabIndex = 14;
            this.gameName.Text = "Rudi Dungeon";
            // 
            // strenghtField
            // 
            this.strenghtField.Location = new System.Drawing.Point(96, 74);
            this.strenghtField.Maximum = new decimal(new int[] {
            16,
            0,
            0,
            0});
            this.strenghtField.Minimum = new decimal(new int[] {
            9,
            0,
            0,
            0});
            this.strenghtField.Name = "strenghtField";
            this.strenghtField.Size = new System.Drawing.Size(45, 20);
            this.strenghtField.TabIndex = 15;
            this.strenghtField.Value = new decimal(new int[] {
            9,
            0,
            0,
            0});
            this.strenghtField.ValueChanged += new System.EventHandler(this.strenghtField_ValueChanged);
            // 
            // braveryField
            // 
            this.braveryField.Location = new System.Drawing.Point(96, 103);
            this.braveryField.Maximum = new decimal(new int[] {
            16,
            0,
            0,
            0});
            this.braveryField.Minimum = new decimal(new int[] {
            9,
            0,
            0,
            0});
            this.braveryField.Name = "braveryField";
            this.braveryField.Size = new System.Drawing.Size(45, 20);
            this.braveryField.TabIndex = 16;
            this.braveryField.Value = new decimal(new int[] {
            9,
            0,
            0,
            0});
            this.braveryField.ValueChanged += new System.EventHandler(this.braveryField_ValueChanged);
            // 
            // dexterityField
            // 
            this.dexterityField.Location = new System.Drawing.Point(96, 134);
            this.dexterityField.Maximum = new decimal(new int[] {
            16,
            0,
            0,
            0});
            this.dexterityField.Minimum = new decimal(new int[] {
            9,
            0,
            0,
            0});
            this.dexterityField.Name = "dexterityField";
            this.dexterityField.Size = new System.Drawing.Size(45, 20);
            this.dexterityField.TabIndex = 17;
            this.dexterityField.Value = new decimal(new int[] {
            9,
            0,
            0,
            0});
            this.dexterityField.ValueChanged += new System.EventHandler(this.dexterityField_ValueChanged);
            // 
            // constitutionField
            // 
            this.constitutionField.Location = new System.Drawing.Point(96, 168);
            this.constitutionField.Maximum = new decimal(new int[] {
            16,
            0,
            0,
            0});
            this.constitutionField.Minimum = new decimal(new int[] {
            9,
            0,
            0,
            0});
            this.constitutionField.Name = "constitutionField";
            this.constitutionField.Size = new System.Drawing.Size(45, 20);
            this.constitutionField.TabIndex = 18;
            this.constitutionField.Value = new decimal(new int[] {
            9,
            0,
            0,
            0});
            this.constitutionField.ValueChanged += new System.EventHandler(this.constitutionField_ValueChanged);
            // 
            // intelligenceField
            // 
            this.intelligenceField.Location = new System.Drawing.Point(96, 205);
            this.intelligenceField.Maximum = new decimal(new int[] {
            16,
            0,
            0,
            0});
            this.intelligenceField.Minimum = new decimal(new int[] {
            9,
            0,
            0,
            0});
            this.intelligenceField.Name = "intelligenceField";
            this.intelligenceField.Size = new System.Drawing.Size(45, 20);
            this.intelligenceField.TabIndex = 19;
            this.intelligenceField.Value = new decimal(new int[] {
            9,
            0,
            0,
            0});
            this.intelligenceField.ValueChanged += new System.EventHandler(this.intelligenceField_ValueChanged);
            // 
            // charismaField
            // 
            this.charismaField.Location = new System.Drawing.Point(96, 244);
            this.charismaField.Maximum = new decimal(new int[] {
            16,
            0,
            0,
            0});
            this.charismaField.Minimum = new decimal(new int[] {
            9,
            0,
            0,
            0});
            this.charismaField.Name = "charismaField";
            this.charismaField.Size = new System.Drawing.Size(45, 20);
            this.charismaField.TabIndex = 20;
            this.charismaField.Value = new decimal(new int[] {
            9,
            0,
            0,
            0});
            this.charismaField.ValueChanged += new System.EventHandler(this.charismaField_ValueChanged);
            // 
            // equipmentField
            // 
            this.equipmentField.Enabled = false;
            this.equipmentField.Location = new System.Drawing.Point(96, 371);
            this.equipmentField.Name = "equipmentField";
            this.equipmentField.Size = new System.Drawing.Size(427, 20);
            this.equipmentField.TabIndex = 26;
            // 
            // nameField
            // 
            this.nameField.Location = new System.Drawing.Point(96, 30);
            this.nameField.Name = "nameField";
            this.nameField.Size = new System.Drawing.Size(173, 20);
            this.nameField.TabIndex = 27;
            this.nameField.Text = "Name";
            this.nameField.TextChanged += new System.EventHandler(this.nameField_TextChanged);
            // 
            // startpointsField
            // 
            this.startpointsField.Enabled = false;
            this.startpointsField.Location = new System.Drawing.Point(529, 30);
            this.startpointsField.Name = "startpointsField";
            this.startpointsField.Size = new System.Drawing.Size(35, 20);
            this.startpointsField.TabIndex = 28;
            this.startpointsField.Text = "7";
            this.startpointsField.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.startpointsField.TextChanged += new System.EventHandler(this.startpointsField_TextChanged);
            // 
            // CharismaName
            // 
            this.CharismaName.AutoSize = true;
            this.CharismaName.Location = new System.Drawing.Point(28, 246);
            this.CharismaName.Name = "CharismaName";
            this.CharismaName.Size = new System.Drawing.Size(50, 13);
            this.CharismaName.TabIndex = 29;
            this.CharismaName.Text = "Charisma";
            // 
            // healthField
            // 
            this.healthField.Enabled = false;
            this.healthField.Location = new System.Drawing.Point(540, 100);
            this.healthField.Name = "healthField";
            this.healthField.Size = new System.Drawing.Size(100, 20);
            this.healthField.TabIndex = 30;
            this.healthField.Text = "0";
            this.healthField.TextChanged += new System.EventHandler(this.healthField_TextChanged);
            // 
            // manaField
            // 
            this.manaField.Enabled = false;
            this.manaField.Location = new System.Drawing.Point(540, 131);
            this.manaField.Name = "manaField";
            this.manaField.Size = new System.Drawing.Size(100, 20);
            this.manaField.TabIndex = 31;
            this.manaField.Text = "0";
            // 
            // intuitionField
            // 
            this.intuitionField.Enabled = false;
            this.intuitionField.Location = new System.Drawing.Point(540, 161);
            this.intuitionField.Name = "intuitionField";
            this.intuitionField.Size = new System.Drawing.Size(100, 20);
            this.intuitionField.TabIndex = 32;
            this.intuitionField.Text = "12";
            // 
            // RaceField
            // 
            this.RaceField.FormattingEnabled = true;
            this.RaceField.Items.AddRange(new object[] {
            "Human",
            "Elb",
            "Dwarf",
            "Orc",
            "Bigfoot",
            "Litlefoot",
            "Eiergeier"});
            this.RaceField.Location = new System.Drawing.Point(288, 87);
            this.RaceField.Name = "RaceField";
            this.RaceField.Size = new System.Drawing.Size(121, 21);
            this.RaceField.TabIndex = 33;
            this.RaceField.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // resetbutton
            // 
            this.resetbutton.Location = new System.Drawing.Point(21, 289);
            this.resetbutton.Name = "resetbutton";
            this.resetbutton.Size = new System.Drawing.Size(120, 39);
            this.resetbutton.TabIndex = 34;
            this.resetbutton.Text = "Reset";
            this.resetbutton.UseVisualStyleBackColor = true;
            this.resetbutton.Click += new System.EventHandler(this.button1_Click);
            // 
            // charPaper
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(674, 421);
            this.Controls.Add(this.resetbutton);
            this.Controls.Add(this.RaceField);
            this.Controls.Add(this.intuitionField);
            this.Controls.Add(this.manaField);
            this.Controls.Add(this.healthField);
            this.Controls.Add(this.CharismaName);
            this.Controls.Add(this.startpointsField);
            this.Controls.Add(this.nameField);
            this.Controls.Add(this.equipmentField);
            this.Controls.Add(this.charismaField);
            this.Controls.Add(this.intelligenceField);
            this.Controls.Add(this.constitutionField);
            this.Controls.Add(this.dexterityField);
            this.Controls.Add(this.braveryField);
            this.Controls.Add(this.strenghtField);
            this.Controls.Add(this.gameName);
            this.Controls.Add(this.IntuitionName);
            this.Controls.Add(this.manaName);
            this.Controls.Add(this.HealtName);
            this.Controls.Add(this.StartpointsName);
            this.Controls.Add(this.raceName);
            this.Controls.Add(this.BraveryName);
            this.Controls.Add(this.dexterityName);
            this.Controls.Add(this.ConstitutionName);
            this.Controls.Add(this.intelligenceName);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.ProfessionName);
            this.Controls.Add(this.strenghtName);
            this.Controls.Add(this.professionField);
            this.Controls.Add(this.createButton);
            this.Name = "charPaper";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.charPaper_Load);
            ((System.ComponentModel.ISupportInitialize)(this.strenghtField)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.braveryField)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dexterityField)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.constitutionField)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.intelligenceField)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.charismaField)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();
            this.ShowDialog();

        }

        #endregion

        private System.Windows.Forms.Button createButton;
        private System.Windows.Forms.ComboBox professionField;
        private System.Windows.Forms.Label strenghtName;
        private System.Windows.Forms.Label ProfessionName;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label intelligenceName;
        private System.Windows.Forms.Label ConstitutionName;
        private System.Windows.Forms.Label dexterityName;
        private System.Windows.Forms.Label BraveryName;
        private System.Windows.Forms.Label raceName;
        private System.Windows.Forms.Label StartpointsName;
        private System.Windows.Forms.Label HealtName;
        private System.Windows.Forms.Label manaName;
        private System.Windows.Forms.Label IntuitionName;
        private System.Windows.Forms.Label gameName;
        private System.Windows.Forms.NumericUpDown strenghtField;
        private System.Windows.Forms.NumericUpDown braveryField;
        private System.Windows.Forms.NumericUpDown dexterityField;
        private System.Windows.Forms.NumericUpDown constitutionField;
        private System.Windows.Forms.NumericUpDown intelligenceField;
        private System.Windows.Forms.NumericUpDown charismaField;
        private System.Windows.Forms.TextBox equipmentField;
        private System.Windows.Forms.TextBox nameField;
        private System.Windows.Forms.TextBox startpointsField;
        private System.Windows.Forms.Label CharismaName;
        private System.Windows.Forms.TextBox healthField;
        private System.Windows.Forms.TextBox manaField;
        private System.Windows.Forms.TextBox intuitionField;
        private System.Windows.Forms.ComboBox RaceField;
        private System.Windows.Forms.Button resetbutton;
    }
}