﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ConsoleApplication2
{
    public partial class charPaper : Form
    {

        private int newstarpoints = 999999999;
      
        private int tempstartp = 7;
        decimal tempoldstr = 9;
        decimal tempolddex = 9;
        decimal tempoldchar = 9;
        decimal tempoldcons = 9;
        decimal tempoldbra = 9;
        decimal tempoldint = 9;

        public charPaper()
        {
            InitializeComponent();
        }

        private void calculateHpMp()
        {
            decimal healthpoints = (strenghtField.Value + dexterityField.Value + constitutionField.Value + braveryField.Value )/2;
            healthpoints = Math.Round(healthpoints); 
            decimal manapoints = (charismaField.Value + intelligenceField.Value + constitutionField.Value +braveryField.Value )/2;
            manapoints = Math.Round(manapoints);
            healthField.Text = healthpoints.ToString();
            manaField.Text = manapoints.ToString();
        }

        private void changeSkillPoints(decimal newvalue , ref decimal oldvalue, NumericUpDown field)
        {

            if (newstarpoints == 0)
            {
                strenghtField.Enabled = false;
                dexterityField.Enabled = false;
                charismaField.Enabled = false;
                constitutionField.Enabled = false;
                braveryField.Enabled = false;
                intelligenceField.Enabled = false;
            }
            else
            {


                if (newvalue > oldvalue)
                {
                    newstarpoints = tempstartp - 1;
                    startpointsField.Text = newstarpoints.ToString();
                    tempstartp = newstarpoints;
                    oldvalue = newvalue;
                }

                if (newvalue < oldvalue)
                {
                    newstarpoints = tempstartp + 1;
                    startpointsField.Text = newstarpoints.ToString();
                    tempstartp = newstarpoints;
                    oldvalue = newvalue;
                }
            }
            
            
            calculateHpMp();

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void charPaper_Load(object sender, EventArgs e)
        {

        }

        private void createButton_Click(object sender, EventArgs e)
        {
            hero createHero = new hero(nameField.Text, strenghtField.Value, dexterityField.Value, constitutionField.Value, braveryField.Value, charismaField.Value, intelligenceField.Value, healthField.Text, manaField.Text, intuitionField.Text, professionField.Text, RaceField.Text);
            this.Visible = false;
        }

        private void nameField_TextChanged(object sender, EventArgs e)
        {

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void healthField_TextChanged(object sender, EventArgs e)
        {

        }

        private void startpointsField_TextChanged(object sender, EventArgs e)
        {

        }

        private void strenghtField_ValueChanged(object sender, EventArgs e)
        {
            changeSkillPoints(strenghtField.Value, ref tempoldstr, strenghtField);
        }

        private void braveryField_ValueChanged(object sender, EventArgs e)
        {
            changeSkillPoints(braveryField.Value, ref tempoldbra, braveryField);
        }

        private void dexterityField_ValueChanged(object sender, EventArgs e)
        {
         
            changeSkillPoints(dexterityField.Value, ref tempolddex, dexterityField);
           
        }

        private void constitutionField_ValueChanged(object sender, EventArgs e)
        {
            changeSkillPoints(constitutionField.Value, ref tempoldcons, constitutionField);
        }

        private void intelligenceField_ValueChanged(object sender, EventArgs e)
        {
            changeSkillPoints(intelligenceField.Value, ref tempoldint, intelligenceField);
        }

        private void charismaField_ValueChanged(object sender, EventArgs e)
        {
            changeSkillPoints(charismaField.Value, ref tempoldchar, charismaField);
        }

        private void button1_Click(object sender, EventArgs e)
        {
        
         newstarpoints = 999999999;
         tempstartp = 7;
         tempoldstr = 9;
         tempolddex = 9;
         tempoldchar = 9;
         tempoldcons = 9;
         tempoldbra = 9;
         tempoldint = 9;

         strenghtField.Enabled = true;
         dexterityField.Enabled = true;
         charismaField.Enabled = true;
         constitutionField.Enabled = true;
         braveryField.Enabled = true;
         intelligenceField.Enabled = true;

         strenghtField.Value = 9;
         braveryField.Value = 9;
         dexterityField.Value = 9;
         constitutionField.Value = 9;
         charismaField.Value = 9;
         intelligenceField.Value = 9;

         startpointsField.Text = "7";
        }

        private void professionField_SelectedIndexChanged(object sender, EventArgs e)
        {
        
        }

    }
}
